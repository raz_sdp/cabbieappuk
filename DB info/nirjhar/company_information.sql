/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : cabbieapp

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-12-19 05:58:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for company_information
-- ----------------------------
DROP TABLE IF EXISTS `company_information`;
CREATE TABLE `company_information` (
  `user_id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `font` varchar(100) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `information_text` text,
  `background_image` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_information
-- ----------------------------
INSERT INTO `company_information` VALUES ('279', '/cabbieappuk/files/vendors/shwapnik.png', 'This is test vendor', null, '#654897', 'We work with licenced hackney carriage taxi offices and drivers, and licenced private hire cab offices to give you the better service. All of our Drivers are Criminal Record Check Approved by their licensing authority. We intend to have licenced taxis working with Cabbie App UK throughout the country so wherever you are travelling in the United Kingdom you\'ll always be able to find a taxi with us. If we do not have a taxi in your area when you need it we have a backup plan and will provide you a local taxi number instead.', '/cabbieappuk/files/vendors/bg.jpg', 'dfdfd', 'dfdfdf', 'dfdfdf');
