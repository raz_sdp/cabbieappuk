/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : cabbieappukv2

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-12-28 19:31:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `company_information`
-- ----------------------------
DROP TABLE IF EXISTS `company_information`;
CREATE TABLE `company_information` (
  `user_id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `logo_icon` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `font` varchar(100) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `background_image` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_information
-- ----------------------------
INSERT INTO `company_information` VALUES ('710', '/files/vendors/shwapnik.png', '/files/vendors/shwapnik.png', 'This is test vendor', null, '#ef4323', 'Shwapnik Event management', '<p>As long as you have \"Cabbie App UK\" you will never have to worry about a taxi number again anywhere in the UK. </p>', '/files/vendors/bg.jpg', 'dfdfd', 'dfdfdf', 'dfdfdf');
