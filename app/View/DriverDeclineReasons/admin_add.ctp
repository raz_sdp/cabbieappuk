<div class="driverDeclineReasons form">
<?php echo $this->Form->create('DriverDeclineReason'); ?>
	<fieldset>
		<legend><?php echo __('Add Driver Decline Reason'); ?></legend>
	<?php
		echo $this->Form->input('reason');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
