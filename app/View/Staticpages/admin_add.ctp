<div class="staticpages form">
<?php echo $this->Form->create('Staticpage'); ?>
	<fieldset>
		<legend><?php echo __('Add New Page'); ?></legend>
	<?php
		echo $this->Form->input('menu_title');
		echo $this->Form->input('title');
		echo $this->Form->input('content', array('type' => 'textarea', 'class' => 'content_page'));
		echo $this->Form->input('sort_order');
		echo $this->Form->input('slug');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
