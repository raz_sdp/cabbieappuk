<div class="staticpages view">
<h2><?php echo __('CMS Page'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sort Order'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['sort_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Menu Title'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['menu_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Slug'); ?></dt>
		<dd>
			<?php echo h($staticpage['Staticpage']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo date_format(date_create($staticpage['Staticpage']['created']),'d/m/Y g:i A'); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>