<div class="holidaytypes index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add new Holiday', array('controller' => 'holidaytypes', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></div>
	<h2><?php echo __('Holidays'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('holiday_type'); ?></th>
			<th><?php echo $this->Paginator->sort('holiday','Date'); ?></th>
			<th><?php echo $this->Paginator->sort('is_repeat','Repeat Every Year'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($holidaytypes as $holidaytype): ?>
	<tr>
		<td><?php echo h($holidaytype['Holidaytype']['holiday_type']); ?>&nbsp;</td>
		<td>
			<?php if(empty($holidaytype['Holidaytype']['is_repeat'])) {
				$dates = array();
				foreach ($holidaytype['Holiday'] as $holiday) {
					$dates[] = date('d/m/Y',strtotime($holiday['holiday']));
				}
				echo implode(', ', $dates);
				} else {
					echo date_format(date_create($holidaytype['Holiday'][0]['holiday']),'d/m');
				}
			?>
		</td>
		<td>
			<?php
			if(!empty($holidaytype['Holidaytype']['is_repeat'])) {
				echo h('Yes'); 
			} else {
				echo h('No');
			}
			?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $holidaytype['Holidaytype']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $holidaytype['Holidaytype']['id']), null, __('Are you sure you want to delete %s?', $holidaytype['Holidaytype']['holiday_type'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>
