<?php
	echo $this->Html->script(array('add_holidays','jquery-ui'));
	echo $this->Html->css(array('jquery-ui', 'style-ui'));
?>
<div class="holidaytypes form">
<?php echo $this->Form->create('Holidaytype'); ?>
	<fieldset>
		<legend><?php echo __('Add Holiday'); ?></legend>
	<?php
		echo $this->Form->input('holiday_type');
		echo $this->Form->input('holiday',array('label' =>'Date', 'name' => 'data[Holiday][holiday][]', 'id' => false, 'class' => 'datePicker', 'style' => 'width:175px; height:30px; padding:0'));
		echo $this->Form->input('Add More Dates',array('type' => 'button', 'label' => false, 'value' =>'Add More Date', 'id' => 'AddMoreDates'));
		echo $this->Form->input('is_repeat',array('type' => 'checkbox', 'label' => 'Repeat Every Year'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>