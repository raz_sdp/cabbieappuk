<div class="bookings form">
<?php echo $this->Form->create('Booking'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Booking'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('pick_up');
		echo $this->Form->input('pick_up_lat');
		echo $this->Form->input('pick_up_lng');
		echo $this->Form->input('destination');
		echo $this->Form->input('destination_lat');
		echo $this->Form->input('destination_lng');
		echo $this->Form->input('at_time');
		echo $this->Form->input('persons');
		echo $this->Form->input('via');
		echo $this->Form->input('fare');
		echo $this->Form->input('is_paid');
		echo $this->Form->input('requester');
		echo $this->Form->input('acceptor');
		echo $this->Form->input('accept_time');
		echo $this->Form->input('area_code_id');
		echo $this->Form->input('special_requirement');
		echo $this->Form->input('bar_driver');
		echo $this->Form->input('note_to_driver');
		echo $this->Form->input('booking_type');
		echo $this->Form->input('is_pre_book');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>