<div class="bookings form">
<?php echo $this->Form->create('Booking'); ?>
	<fieldset>
		<legend><?php echo __('Edit Booking'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('pick_up');
		echo $this->Form->input('pick_up_lat');
		echo $this->Form->input('pick_up_lng');
		echo $this->Form->input('destination');
		echo $this->Form->input('destination_lat');
		echo $this->Form->input('destination_lng');
		echo $this->Form->input('at_time');
		echo $this->Form->input('persons');
		echo $this->Form->input('via');
		echo $this->Form->input('fare');
		echo $this->Form->input('is_paid');
		echo $this->Form->input('requester');
		echo $this->Form->input('acceptor');
		echo $this->Form->input('accept_time');
		echo $this->Form->input('special_requirement');
		echo $this->Form->input('bar_driver');
		echo $this->Form->input('note_to_driver');
		echo $this->Form->input('is_app_to_app');
		echo $this->Form->input('is_vr');
		echo $this->Form->input('is_pre_book');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Booking.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Booking.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bookings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
