<div class="bookings view">
<h2><?php  echo __('Booking'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['pick_up']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Lat'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['pick_up_lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pick Up Lng'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['pick_up_lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['destination']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Lat'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['destination_lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination Lng'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['destination_lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('At Time'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['at_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Persons'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['persons']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Via'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['via']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fare'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['fare']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Paid'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['is_paid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requester'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['requester']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acceptor'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['acceptor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accept Time'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['accept_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Special Requirement'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['special_requirement']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bar Driver'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['bar_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note To Driver'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['note_to_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is App To App'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['is_app_to_app']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Vr'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['is_vr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Pre Book'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['is_pre_book']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Booking'), array('action' => 'edit', $booking['Booking']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Booking'), array('action' => 'delete', $booking['Booking']['id']), null, __('Are you sure you want to delete # %s?', $booking['Booking']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bookings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking'), array('action' => 'add')); ?> </li>
	
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Billings'); ?></h3>
	<?php if (!empty($booking['Billing'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Promoter Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($booking['Billing'] as $billing): ?>
		<tr>
			<td><?php echo $billing['id']; ?></td>
			<td><?php echo $billing['user_id']; ?></td>
			<td><?php echo $billing['promoter_id']; ?></td>
			<td><?php echo $billing['booking_id']; ?></td>
			<td><?php echo $billing['amount']; ?></td>
			<td><?php echo $billing['created']; ?></td>
			<td><?php echo $billing['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'billings', 'action' => 'view', $billing['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'billings', 'action' => 'edit', $billing['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'billings', 'action' => 'delete', $billing['id']), null, __('Are you sure you want to delete # %s?', $billing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Rejects'); ?></h3>
	<?php if (!empty($booking['Reject'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('Rejected Time'); ?></th>
		<th><?php echo __('Rejected By'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($booking['Reject'] as $reject): ?>
		<tr>
			<td><?php echo $reject['id']; ?></td>
			<td><?php echo $reject['user_id']; ?></td>
			<td><?php echo $reject['booking_id']; ?></td>
			<td><?php echo $reject['rejected_time']; ?></td>
			<td><?php echo $reject['rejected_by']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rejects', 'action' => 'view', $reject['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rejects', 'action' => 'edit', $reject['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rejects', 'action' => 'delete', $reject['id']), null, __('Are you sure you want to delete # %s?', $reject['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Request Logs'); ?></h3>
	<?php if (!empty($booking['RequestLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($booking['RequestLog'] as $requestLog): ?>
		<tr>
			<td><?php echo $requestLog['id']; ?></td>
			<td><?php echo $requestLog['booking_id']; ?></td>
			<td><?php echo $requestLog['user_id']; ?></td>
			<td><?php echo $requestLog['created']; ?></td>
			<td><?php echo $requestLog['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'request_logs', 'action' => 'view', $requestLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'request_logs', 'action' => 'edit', $requestLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'request_logs', 'action' => 'delete', $requestLog['id']), null, __('Are you sure you want to delete # %s?', $requestLog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
