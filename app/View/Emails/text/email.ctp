<body>    
 <div class="container" style="margin-right: auto;margin-left: auto;padding-left: 15px;padding-right:15px;">
  <div class="emailcontainer" style="background: #fff; width: 740px; height: 550px; margin:auto; padding-top: 20px; ">
    <div class="maincont" style="background: #ffd800;width: 640px; height: 480px;margin:auto; "> 
      <h1 style="margin-left: 20px;padding-top: 32px;">Thanks for registering with CabbieAppUK!</h1>
      <div>
        <img  src="<?php echo $this->Html->url('/img/email-logo.png', true); ?>" style=" margin-left: 245px;margin-top: 6px;" alt=""/>    

      </div>          
      <div style=" margin-top: 29px; ">
        <label style=" margin-left: 38px; ">You can now login to your account at the website www.cabbieappuk.com or via our</label>
        <br><label style=" margin-left: 38px;">mobile app - just search CabbieAppUK in the App Store!</label>
        <br>
        <br>
        <br>
        <label style=" margin-left: 38px; ">
          We hope you enjoy using CabbieAppUK and never get stuck without a taxi again!</label>
        </div>
        <div>
          <a href="http://74.3.255.228/dev.cabbieapp"> <img  src="<?php echo $this->Html->url('/img/login.png', true); ?>" style=" margin-left: 185px; margin-top: 35px; " alt=""/> </a>                
        </div>
      </div>
    </div>
  </div>
</body>
