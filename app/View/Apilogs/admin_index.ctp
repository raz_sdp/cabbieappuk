<div class="apilogs index">
  <h2><?php echo __('Apilog'); ?></h2>
  <table cellpadding="0" cellspacing="0">
    <tr>
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('request_url'); ?></th>
        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
        <th><?php echo $this->Paginator->sort('ip_address'); ?></th>
        <th><?php echo $this->Paginator->sort('status'); ?></th>
        <th><?php echo $this->Paginator->sort('time'); ?></th>
        <th><?php echo $this->Paginator->sort('payload')?></th>
        <th><?php echo $this->Paginator->sort('response')?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
    </tr>
    <?php foreach ($apilogs as $apilog): ?>
    <tr>
      <td><?php echo h($apilog['Apilog']['id']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['request_url']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['user_id']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['ip_address']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['status']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['time']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['payload']); ?>&nbsp;</td>
      <td><?php echo h($apilog['Apilog']['response']); ?>&nbsp;</td>
      <td><?php echo  $this->Time->timeAgoInWords($apilog['Apilog']['created']); ?>&nbsp;</td>
    </tr>
<?php endforeach; ?>
  </table>
  <p>
  <?php
  echo $this->Paginator->counter(array(
  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
  ?>
  </div>
</div>
<?php echo $this->element('menu'); ?>