<div class="phoneQueues index">
	<h2><?php echo __('Call Logs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('call_sid'); ?></th>
			<th><?php echo $this->Paginator->sort('from'); ?></th>
			<th><?php echo $this->Paginator->sort('to'); ?></th>
			<th><?php echo $this->Paginator->sort('created', 'Request Time'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php //pr($phoneQueues); ?>
	<?php foreach ($phoneQueues as $phoneQueue): ?>
	<tr>
		<td><?php echo h($phoneQueue['PhoneQueue']['call_sid']); ?>&nbsp;</td>
		<td><?php echo h($phoneQueue['PhoneQueue']['from']); ?>&nbsp;</td>
		<td><?php echo h($phoneQueue['PhoneQueue']['to']); ?>&nbsp;</td>
		<td><?php
			echo date_format(date_create($phoneQueue['PhoneQueue']['created']), 'd/m/Y g:i A');
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Queue'), array('action' => 'view', $phoneQueue['PhoneQueue']['call_sid'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $phoneQueue['PhoneQueue']['id']), null, __('Are you sure you want to delete # %s?', $phoneQueue['PhoneQueue']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>