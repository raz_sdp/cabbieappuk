<div class="phoneQueues index">
	<h2><?php echo __('Record of Call SID '); echo ($phoneQueues[0]['PhoneQueue']['call_sid']); ?></h2>
	<h3><?php echo __('From '); echo ($phoneQueues[0]['PhoneQueue']['from']); ?></h3>
	<h3><?php echo __('To '); echo ($phoneQueues[0]['PhoneQueue']['to']); ?></h3>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<!-- <th><?php // echo $this->Paginator->sort('zone_id'); ?></th> -->
			<!-- <th><?php // echo $this->Paginator->sort('from'); ?></th>
			<th><?php // echo $this->Paginator->sort('to'); ?></th> -->
			<th><?php echo $this->Paginator->sort('driver'); ?></th>
			<th><?php echo $this->Paginator->sort('phone_no'); ?></th>
			<th><?php echo $this->Paginator->sort('created', 'Request Time'); ?></th>
			<th><?php echo $this->Paginator->sort('dial_call_duration'); ?></th>
			<th><?php echo $this->Paginator->sort('recording_url'); ?></th>
			<th><?php echo $this->Paginator->sort('completed'); ?></th>
	</tr>
	<?php // pr($phoneQueues); ?>
	<?php foreach ($phoneQueues as $phoneQueue): ?>
	<tr>
		<!-- <td><?php // echo h($phoneQueue['Zone']['name']); ?>	&nbsp;</td> -->
		<!-- <td><?php // echo h($phoneQueue['PhoneQueue']['from']); ?>	&nbsp;</td>
		<td><?php // echo h($phoneQueue['PhoneQueue']['to']); ?>&nbsp;</td> -->
		<td><?php
		if(empty($phoneQueue['User']['name']) && !empty($phoneQueue['PhoneQueue']['phone_no'])) echo 'Call Driverted';
		elseif (empty($phoneQueue['User']['name']) && empty($phoneQueue['PhoneQueue']['phone_no'])) {
			echo 'No driver found';
		}
		else echo h($phoneQueue['User']['name']); 
		?>&nbsp;</td>
		<td><?php echo h($phoneQueue['PhoneQueue']['phone_no']); ?>&nbsp;</td>
		<td><?php
			echo date_format(date_create($phoneQueue['PhoneQueue']['created']), 'd/m/Y g:i A');
		?>&nbsp;</td>
		<td><?php
			if(!empty($phoneQueue['PhoneQueue']['dial_call_duration'])){
				echo h($phoneQueue['PhoneQueue']['dial_call_duration'] .' sec'); 	
			}else echo 'N/A'
			
		?>&nbsp;</td>
		<td><?php
		if(!empty($phoneQueue['PhoneQueue']['recording_url'])) {
			echo $this->Html->link('Play', $phoneQueue['PhoneQueue']['recording_url']); 
		} else {
			echo 'N/A';
		}
		?>&nbsp;</td>
		<?php if(!empty($phoneQueue['PhoneQueue']['completed'])) {
			$c_time = date_create($phoneQueue['PhoneQueue']['completed']);
			?>
			<td><?php echo date_format($c_time, 'd/m/Y g:i A');  ?>&nbsp;</td> 
		<?php } else {
			?><td><?php echo 'N/A'; ?></td>	
		<?php } ?> 
		
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>