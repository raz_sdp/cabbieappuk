<div class="phoneQueues form">
<?php echo $this->Form->create('PhoneQueue'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Phone Queue'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('call_sid');
		echo $this->Form->input('zone_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('request_time');
		echo $this->Form->input('response_time');
		echo $this->Form->input('response');
		echo $this->Form->input('user_comment');
		echo $this->Form->input('driver_decline_reason_id');
		echo $this->Form->input('is_reactivated');
		echo $this->Form->input('pob_time');
		echo $this->Form->input('ondoor_time');
		echo $this->Form->input('no_show_time');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PhoneQueue.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('PhoneQueue.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Phone Queues'), array('action' => 'index')); ?></li>
	</ul>
</div>
