<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->script(array('jquery-1.8.2.min', 'redactor'));
		echo $this->Html->css(array('cake.generic', 'zones', 'site', 'redactor', 'bootstrap.min'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script>
	var ROOT = '<?php echo $this->Html->url('/', true); ?>';
    var HERE = '<?php echo $this->here; ?>';
	</script>
	<script type="text/javascript">
	$(document).ready(
		function()
		{
			$('.content_page').redactor();
		}
	);
	</script>




</head>
<body>
	<div id="container">
		<div id="header">
			<h1>Cabbie App</h1>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
