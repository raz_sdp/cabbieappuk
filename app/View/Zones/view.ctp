<div class="zones view">
<h2><?php  echo __('Zone'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Polygon Points'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['polygon_points']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Center'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['center']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Radius'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['radius']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($zone['Zone']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Zone'), array('action' => 'edit', $zone['Zone']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Zone'), array('action' => 'delete', $zone['Zone']['id']), null, __('Are you sure you want to delete # %s?', $zone['Zone']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Zones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zone'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queues'), array('controller' => 'queues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Queues'); ?></h3>
	<?php if (!empty($zone['Queue'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Zone Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Position'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($zone['Queue'] as $queue): ?>
		<tr>
			<td><?php echo $queue['id']; ?></td>
			<td><?php echo $queue['zone_id']; ?></td>
			<td><?php echo $queue['user_id']; ?></td>
			<td><?php echo $queue['position']; ?></td>
			<td><?php echo $queue['created']; ?></td>
			<td><?php echo $queue['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'queues', 'action' => 'view', $queue['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'queues', 'action' => 'edit', $queue['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'queues', 'action' => 'delete', $queue['id']), null, __('Are you sure you want to delete # %s?', $queue['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
