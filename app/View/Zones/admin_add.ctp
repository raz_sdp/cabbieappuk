<script type="text/javascript">
	// zone type selection drop down change fn
	$(function(){
		$('#ZoneType').on('change', function(){
			location.href = ROOT + 'admin/zones/add/' + $(this).val();
		});
	});
</script>
<?php
	$zone_types = array(
				'vr_zone' => 'VR Zone',
				'phonecall_gps_zone'=> 'Phone GPS Zone',
				'phonecall_zone' => 'Phonecall Zone'
				
			);
 if($zone_type != 'phonecall_zone'){ ?>
<script type="text/javascript">
	var polygons = JSON.parse('<?php echo json_encode($polygons);?>');
	</script>
<?php } 
echo $this->Html->script(array('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry&sensor=false','polygon.min','markerwithlabel_packed','zones_add')); ?>

<div class="zones form">
  <h2><?php echo __('Admin Add '); ?> <?php echo($zone_types[$zone_type]); ?></h2>
  <div id="mapView">
	<?php if($zone_type != 'phonecall_zone') { ?>
    <table style="width:200px;">
		<tr>
			<td><div id="hand_b"/></td>
			<td><div id="shape_b"/></td>	
			<td style="width:390px">
			<form id="options">
				<!--<label for="radiusInput">Radius</label> <input type="text" value="5" id="radiusInput" name="radiusInput"> km-->
			</form></td>
		</tr>
    </table>
    <?php } ?>
	<?php echo $this->Form->create('Zone');

		if($zone_type != 'phonecall_zone') {
	?>
	<fieldset>
		
	<!-- map	-->
	<div id="main-map">
	</div>
	<?php
	}
			echo $this->Form->input('name', array('style' => 'width:250px; height: 20px'));
			if($zone_type == 'vr_zone') {
	        echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds'));
	        echo $this->Form->input('pickup_lead_time', array('label' => 'How many minutes before the pick up time the job should be offered to the driver'));
	        echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN', 'checked' => true));
	        echo $this->Form->input('reject_threshold', array('label' => 'Log Drivers OUT after number of Ignore or Reject Jobs'));
	        echo $this->Form->input('allow_inactive_time', array('label' => 'Log Drivers Out after number of Inactive Minute'));
	    	}
			if($zone_type != 'vr_zone') {
				echo $this->Form->input('voip_phone_no',array('options'=>$existing_purchase_twilio_numbers,'style' => 'width:250px; height: 20px','label' => 'VOIP Phone No'));
			}
			echo $this->Form->input('type', array('options' => $zone_types,
			'selected' => $zone_type
			));
		?>

	<?php
		echo $this->Form->input('polygon', array('type' => 'hidden'));
		echo $this->Form->input('polygon_points', array('type' => 'hidden'));
	?>

	</fieldset>
	<?php echo $this->Form->end('Submit'); ?> 
	</div>
</div>
<?php echo $this->element('menu'); ?>
