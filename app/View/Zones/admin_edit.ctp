<div class="zones form">
<?php echo $this->Form->create('Zone'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Zone'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('type', array('type' => 'hidden'));
		echo $this->Form->input('name',array('style' => 'width: 250px; height: 20px' ));
		if($this->request->data['Zone']['type'] != 'vr_zone') {
		echo $this->Form->input('voip_phone_no',array('options'=> $existing_purchase_twilio_numbers,'style' => 'width:250px; height: 20px'));
		}	
		if($this->request->data['Zone']['type'] == 'vr_zone') {
	        echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds'));
	        echo $this->Form->input('pickup_lead_time', array('label' => 'How many minutes before the pick up time the job should be offered to the driver'));
	        echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN', 'checked' => true));
	        echo $this->Form->input('reject_threshold', array('label' => 'Log Drivers OUT after number of Ignore or Reject Jobs'));
	        echo $this->Form->input('allow_inactive_time', array('label' => 'Log Drivers Out after number of Inactive Minute'));
	    	}
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
