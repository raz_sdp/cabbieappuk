<div class="phoneSettings index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add New Booking Setting', array('controller' => 'phone_settings', 'action' => 'add', 'admin' => true));?></div>
	<h2><?php echo __('Settings for Phonecall and Phone GPS Zone'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>	
			<th><?php echo $this->Paginator->sort('zone_id'); ?></th>
			<th><?php echo $this->Paginator->sort('req_timeout', 'Try Each Logged In Driver for Number of Seconds'); ?></th>
			<th><?php echo $this->Paginator->sort('remain_loggedin_on_accept', 'After Accepting a Job Leave Drivers Logged IN'); ?></th>
			<!-- <th><?php // echo $this->Paginator->sort('reject_threshold'); ?></th> -->
			<!-- <th><?php // echo $this->Paginator->sort('min_call_duration'); ?></th> -->
			<th><?php echo $this->Paginator->sort('allow_inactive_time', 'Allowed Inactive Time(minute)'); ?></th>
			<th><?php echo $this->Paginator->sort('minimum_charge'); ?></th>
			<th><?php echo $this->Paginator->sort('per_minute_charge'); ?></th>
			<th><?php echo $this->Paginator->sort('repeat_call_timeout', 'Route Repeat Calls to the Same Driver Within(minute)'); ?></th>
			<th><?php echo $this->Paginator->sort('bar_private_no', 'Bar all Incoming Private & Withheld Numbers'); ?></th>
			<th><?php echo $this->Paginator->sort('divert_phoneno','Divert the call'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($phoneSettings as $phoneSetting): ?>
	<tr>
		<td><?php if(!empty($phoneSetting['Zone']['name'])) {
			echo h($phoneSetting['Zone']['name']); 
		} else {
			echo 'Default';
		}
		?>&nbsp;</td>
		<td><?php echo h($phoneSetting['PhoneSetting']['req_timeout']); ?>&nbsp;</td>
		<td><?php 
		if(empty($phoneSetting['PhoneSetting']['remain_loggedin_on_accept'])) {
			echo ('No');
		} else
		echo h('Yes'); 
		?>&nbsp;
		</td>
		<!-- <td><?php //echo h($phoneSetting['PhoneSetting']['reject_threshold']); ?>&nbsp;</td> -->
		<!-- <td><?php // echo h($phoneSetting['PhoneSetting']['min_call_duration']); ?>&nbsp;</td> -->
		<td><?php
		if(!empty($phoneSetting['PhoneSetting']['allow_inactive_time'])){
			echo h($phoneSetting['PhoneSetting']['allow_inactive_time']);
		} else echo h('N/A');
		  ?>&nbsp;
		</td>
		<td><?php echo '£ '.$phoneSetting['PhoneSetting']['minimum_charge']; ?>&nbsp;</td>
		<td><?php echo '£ '.$phoneSetting['PhoneSetting']['per_minute_charge']; ?>&nbsp;</td>
		<td><?php
		if(!empty($phoneSetting['PhoneSetting']['repeat_call_timeout'])) {
		 	echo h($phoneSetting['PhoneSetting']['repeat_call_timeout']);
		} else echo h('N/A');
		?>&nbsp;</td>
		<td><?php 
		if(empty($phoneSetting['PhoneSetting']['bar_private_no'])) {
			echo ('No');
		} else
		echo h('Yes'); 
		?>&nbsp;
		</td>
		<td><?php
		if (!empty($phoneSetting['PhoneSetting']['divert_phoneno']))
			echo h($phoneSetting['PhoneSetting']['divert_phoneno']); 
		else echo 'N/A'
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $phoneSetting['PhoneSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $phoneSetting['PhoneSetting']['id']), null, __('Are you sure you want to delete %s?', $phoneSetting['Zone']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>
