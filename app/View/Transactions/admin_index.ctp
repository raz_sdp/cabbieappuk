<div class="transaction index">
	<div class="AddNewButton"><?php echo $this->Html->link('View Admin Fee Record ', array('action' => 'paymentrecord', 'admin' => true, 'prefix' => 'admin'))?></div>
	<h2><?php echo __('Transactions'); ?></h2>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('amount', 'Balance (Available Balance with Free Credit)'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php // pr($transactions); ?>
	<?php foreach ($transactions as $transaction): ?>
	<tr>
		<td>
			<?php echo h($transaction['User']['name'] .' ('. $transaction['User']['type'].')'); ?>
		</td>
		<td><?php echo $this->Number->currency($transaction[0]['tran_amount'], 'GBP'); 
		?>&nbsp;
		</td>
		<td class="actions">
			<?php // echo $this->Html->link(__('Add'), array('controller' => 'top_ups', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $transaction['TopUp']['user_id'])); ?>
			<?php echo $this->Html->link(__('View'), array('controller' => 'users','admin' => true, 'action' => 'admin_topup', $transaction['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>