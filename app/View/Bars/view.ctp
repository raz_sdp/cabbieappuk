<div class="bars view">
<h2><?php  echo __('Bar'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserId1'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['userId1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserId2'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['userId2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Requested'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['is_requested']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bar'), array('action' => 'edit', $bar['Bar']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bar'), array('action' => 'delete', $bar['Bar']['id']), null, __('Are you sure you want to delete # %s?', $bar['Bar']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bars'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bar'), array('action' => 'add')); ?> </li>
	</ul>
</div>
