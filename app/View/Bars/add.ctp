<div class="bars form">
<?php echo $this->Form->create('Bar'); ?>
	<fieldset>
		<legend><?php echo __('Add Bar'); ?></legend>
	<?php
		echo $this->Form->input('userId1');
		echo $this->Form->input('userId2');
		echo $this->Form->input('is_requested');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Bars'), array('action' => 'index')); ?></li>
	</ul>
</div>
