<div class="bars view">
<h2><?php  echo __('Bar'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserId1'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['userId1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserId2'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['userId2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Requested'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['is_requested']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($bar['Bar']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>