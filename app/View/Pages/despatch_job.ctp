<script type="text/javascript">
    <?php if(empty($auth)){ ?>
    var loged_in = false;
    <?php } else { ?>
    var loged_in = true;
    <?php } ?>
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    #map-canvas {
        margin: 0;
        width: 100%;
        height: 500px;
    }
    .red-taxi{
        color: red;
    }
    .green-taxi{
        color: green;
    }
</style>


<?php
//echo '<pre>';
//print_r($driver_list);
//echo '</pre>';
foreach ($driver_list as $driver) {
    $latlng[] = [
        'lat'=>$driver['User']['lat'],
        'lng'=>$driver['User']['lng'],
        'color'=>'red'
    ];
}
$js_latlng = json_encode($latlng);

echo $this->Html->script(array('popup_login', 'popup_signup', 'passenger_booking'));
?>
<div id="overlay">
    <div class="wait-message" style="display:none">
        <h2>Please wait...</h2>
    </div>
</div>
<div class="container-fluid">
    <div class="" id="">
        <div class="container">
            <div class="row">
                <div class="icon">
                    <?php echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>

                    <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>

                    <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>

                    <a target="_blank" href="<?php echo $this->Html->url('/admin/users/login');?>" class="login-link">Login</a>

                </div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpt_cHVYOuA_WAmkdjLZ33g9bHrCmXEj8&v=3&libraries=geometry,places"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->Html->url('/map/map-icons.css');?>">
    <script type="text/javascript" src="<?php echo $this->Html->url('/map/map-icons.js');?>"></script>

    <!-- Example -->
    <script type="text/javascript">
        function initialise() {
            var mapCanvas = document.getElementById('map-canvas');

            // Center
            var center = new google.maps.LatLng(22.91750869750977, 88.00411315917969);
            // Map Options
            var mapOptions = {
                zoom: 14,
                center: center,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
//                styles: [
//                    {stylers: [{ visibility: 'simplified' }]},
//                    {elementType: 'labels', stylers: [{ visibility: 'off' }]}
//                ]
            };

            // Create the Map
            map = new google.maps.Map(mapCanvas, mapOptions);
            var js_latlng = $.parseJSON('<?php echo $js_latlng; ?>');
            var positions = [];
            $(js_latlng).each(function(i,val){
                var marker = new Marker({
                    map: map,
                    position: new google.maps.LatLng(val.lat, val.lng),
                    icon: {
                        path: ' ',
                        fillColor: '#6331AE',
                        fillOpacity: 1,
                        strokeColor: '',
                        strokeWeight: 0
                    },
                    map_icon_label: '<span class="map-icon map-icon-taxi-stand" style="color: '+ val.color +'; text-shadow: 0 0 0px #000;"></span>'
                });
            });
        };

        google.maps.event.addDomListener(window, 'load', initialise);
    </script>

    <div class="section-two" id="second">
        <div class="text-form" style="padding-top: 0;">
            <div id="map-canvas"></div>
            <div class="container ">
                <div class="row">
                    <div class="bg-color">
                        <form class="form-horizontal" role="form">
                            <div class="part-one row">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label class="col-sm-4 col-xs-5 control-label label-left">
                                        <?php echo $this->Html->image('location.png', array('alt' => 'CakePHP', 'id'=>'loc')) ?>
                                        Pick Up</label>
                                    <div class="col-sm-8 col-xs-7">
                                        <input onkeyup="addNewAddress('pickup');" autocomplete="off" type="text" class="form-control postcode-auto-complete" id="pickup" placeholder="Pick Up">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-right destina-tion">
                                    <label class="col-sm-5 col-md-5 col-xs-6 control-label">
                                        <?php echo $this->Html->image('location.png', array('alt' => 'CakePHP', 'id'=>'loc1')) ?>
                                        Destination</label>
                                    <div class="col-sm-7 col-xs-6 col-md-7 padding-right">
                                        <input onkeyup="addNewAddress('destination');" autocomplete="off" type="text" id="destination" class="form-control postcode-auto-complete destination" placeholder="Destination">
                                    </div>
                                </div>
                                <input type="hidden" id="lat_1">
                                <input type="hidden" id="lat_2">
                                <input type="hidden" id="lng_1">
                                <input type="hidden" id="lng_2">
                                <script type="text/javascript">
                                    function addNewAddress(location) {
                                        var text = { componentRestrictions: {} };
                                       
                                        var input = document.getElementById(location);
                                        autocomplete = new google.maps.places.Autocomplete(input);                                        
                                        
                                    }

                                    $('#pickup').blur(function () {
                                        if ($(this).val() && $('#destination').val()) {
                                            getLatLng();
                                        }
                                    });
                                    $('#destination').blur(function () {
                                        if ($(this).val() && $('#pickup')) {
                                            getLatLng();
                                        }
                                    });

                                    function getLatLng() {
                                        if ($('#pickup').val() && $('#destination').val()) {
                                            var loc1 = [];
                                            var loc2 = [];
                                            var geocoder = new google.maps.Geocoder();
                                            geocoder.geocode({'address': $('#pickup').val()}, function postcodesearch(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                console.log(results)
                                                    var lat1 = results[0].geometry.location.lat();
                                                    var lng1 = results[0].geometry.location.lng();
                                                    $('#lat_1').val(lat1);
                                                    $('#lng_1').val(lng1);
                                                }
                                                var geocoder2 = new google.maps.Geocoder();
                                                geocoder2.geocode({'address': $('#destination').val()}, function postcodesearch(results, status) {
                                                    if (status == google.maps.GeocoderStatus.OK) {
                                                        var lat2 = results[0].geometry.location.lat();
                                                        var lng2 = results[0].geometry.location.lng();
                                                        $('#lat_2').val(lat2);
                                                        $('#lng_2').val(lng2);
                                                    }
                                                });
                                                if ($('#lat_1').val() && $('#lat_2').val()) {
                                                    distance($('#lat_1').val(), $('#lat_2').val(),$('#lng_1').val(), $('#lng_2').val());
                                                } else {
                                                    getLatLng();
                                                }

                                            });
                                        }
                                    }

                                    var rad = function (x) {
                                        return x * Math.PI / 180;
                                    };
                                    var d = 0;

                                    function distance(lat1, lat2, lng1, lng2) {
//                                        var R = 6378137; // Earth’s mean radius in meter
                                        var R = 6378.16; // Earth’s mean radius in miles
                                        var dLat = rad(lat2 - lat1);
                                        var dLong = rad(lng2 - lng1);
                                        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                                            Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
                                                Math.sin(dLong / 2) * Math.sin(dLong / 2);
                                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                                        d = R * c;
//                                        console.log(d)
                                        $('#js_total_miles').val(d);
                                        //return d; // returns the distance in meter

                                    }
                                </script>
                            </div>
                            <div class="part-one row">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="col-sm-2 col-xs-5 control-label label-left">
                                        <?php echo $this->Html->image('add.png', array('alt' => 'CakePHP', 'id'=>'loc')) ?>
                                        Travel Via</label>
                                    <label class="col-sm-2 col-xs-3 control-label label-right">
                                        Total Miles</label>
                                    <div class="col-sm-3 col-xs-6">
                                        <input autocomplete="off"  type="text" class="form-control" id="js_total_miles" placeholder="0.0">
                                    </div>
                                    <label class="col-sm-2 col-xs-3 control-label label-right">
                                        Estimate Fare</label>
                                    <div class="col-sm-3 col-xs-6">
                                        <input autocomplete="off"  type="text" class="form-control" id="" placeholder="0.00">
                                    </div>
                                </div>
                            </div>
                            <div class="part-tow row">
                                <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right">

                                    <div class="checkbox" id="asap">
                                        <label>
                                            <?php echo $this->Html->image('date.png', array('alt' => 'CakePHP', 'id'=>'datepic')) ?>
                                            <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                            <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                            <input autocomplete="off"  type="checkbox" value="1"> Soon as possible
                                        </label>

                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right Date">
                                    <label class="col-sm-4 col-xs-5 control-label label-left">
                                        Date</label>
                                    <div class="col-sm-8 col-xs-7 date-input">
                                        <input autocomplete="off"  type="text" class="form-control" id="datetext" placeholder="Date">
                                    </div>

                                </div>
                                <div class="form-group col-md-2 col-sm-3 col-xs-12 padding-right time">

                                    <label class="col-sm-4 col-xs-5 control-label label-left">
                                        Time</label>
                                    <div class="col-sm-8 col-xs-7 time-input">
                                        <input autocomplete="off"  type="text" class="form-control" id="timetext" name="pick_up_time" placeholder="Pick Up Time">
                                    </div>

                                </div>
                                <div class="form-group col-md-3 col-sm-3 col-xs-12 padding-right pessengers">

                                    <label class="col-sm-6 col-md-7 col-xs-5 control-label label-left">

                                        No of pessengers</label>
                                    <div class="col-sm-6 col-md-5 col-xs-5">
                                        <select class="form-control" id="passngrno">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group col-md-3 col-sm-3 col-xs-12 Luggage">

                                    <label class="col-sm-6 col-xs-5 control-label label-left">
                                        No of Luggage</label>
                                    <div class="col-sm-6 col-xs-7">
                                        <select class="form-control" id="luggage" >
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="part-three row">
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">
                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox" id="1seat">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic', 'style' => 'display : none;')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check', 'style' => 'display : inline;')) ?>
                                                <input autocomplete="off" id="1seat"  type="checkbox" checked="checked" value="4 Seats"> 1-4 Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">

                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="5 Seats"> 5 Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">
                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="6 Seats"> 6 Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">
                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="7 Seats"> 7 Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">
                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="8 Seats"> 8 Seats
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">

                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="Estate"> Estate
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2 col-sm-4 col-xs-6">
                                    <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo $this->Html->image('checkbox1.png', array('alt' => 'CakePHP', 'class' => 'checkpic')) ?>
                                                <?php echo $this->Html->image('checkbox.png', array('alt' => 'CakePHP', 'class' => 'tick-check')) ?>
                                                <input autocomplete="off"  type="checkbox" value="Wheelchair"> Wheelchair
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="part-one row">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="col-sm-5 col-md-3 col-xs-5 control-label label-left">
                                        <?php echo $this->Html->image('qstn.png', array('alt' => 'CakePHP', 'id'=>'note')) ?>
                                        Additional Note to driver</label>
                                    <div class="col-sm-7 col-md-9 col-xs-7 padding-right">
                                        <input autocomplete="off"  type="email" class="form-control" id="addNote" placeholder="Note to driver">
                                    </div>
                                </div>
                            </div>
                            <div class="part-tow row">
                                <div class="form-group col-md-3 padding-right">
                                    <div class="" id="">
                                        <label>
                                            <?php echo $this->Html->image('qstn.png', array('alt' => 'CakePHP', 'id'=>'datepic')) ?>
                                            Send Job Request to
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 text-left">
                                    <div class="checkbox">
                                        <label>
                                            <input class="check2ndtype" type="checkbox" value="" style="opacity: 1;">
                                            <input autocomplete="off"  type="text" class="form-control" id="" placeholder="Write Driver's number">
                                        </label>
                                    </div>
                                    <span style="color: #fff; margin-left: 20px;">Then</span>
                                    <div class="checkbox">
                                        <label style="font-size: 13px; font-weight: bold"><input class="check2ndtype" type="checkbox" value="" style="opacity: 1;">By VR Zone Position</label>
                                    </div>
                                    <span style="color: #fff; margin-left: 20px;">Then</span>
                                    <div class="checkbox">
                                        <label style="font-size: 13px; font-weight: bold"><input class="check2ndtype" type="checkbox" value=""  style="opacity: 1;">Nearest Driver</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 padding-right">
                                    <br/>
                                    <button type="submit" class="footer_button buy-now" style="box-shadow: 0px 0px 15px #ffd800;"> SEND </button>
                                </div>
                            </div>
                            <div class="last-part row">
                                <div class="col-md-12 text-left">
                                    <label>Send Message to Driver</label>
                                    <textarea class="form-control" name="" style="background-color: #e4b057; background-image: none;"></textarea>
                                </div>
                                <div class="col-md-6 text-left">
                                    <div class="checkbox">
                                        <label style="font-size: 15px; font-weight: bold">
                                            <input class="check2ndtype" type="checkbox" value="" style="opacity: 1;">To all Drivers</label>
                                    </div>
                                </div>
                                <div class="row"></div>
                                <div class="col-md-6 text-left">
                                    <div class="checkbox">
                                        <label style="font-size: 15px; font-weight: bold">
                                            <input class="check2ndtype" type="checkbox" value="" style="opacity: 1;">To all login Drivers</label>
                                    </div>
                                </div>
                                <div class="row"></div>
                                <div class="col-md-6 text-left">
                                    <div class="checkbox">
                                        <label style="font-size: 15px; font-weight: bold">
                                            <input class="check2ndtype" type="checkbox" value="" style="opacity: 1;">To listed drivers in box</label>
                                        <input autocomplete="off"  type="text" class="form-control col-md-4" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <br/>
                                    <button type="submit" class="footer_button" style="box-shadow: 0px 0px 15px #ffd800;"> SEND </button>
                                </div>
                                <div class="row"></div>
                            </div>
                        </form>
                    </div>
                    <div class="row"></div>
                    <!--<style>-->
                    <!--    .check2ndtype{-->
                    <!--        cursor: pointer;-->
                    <!--        -webkit-appearance: none;-->
                    <!--        appearance: none;-->
                    <!--        background: #34495E;-->
                    <!--        border-radius: 1px;-->
                    <!--        box-sizing: border-box;-->
                    <!--        position: relative;-->
                    <!--        box-sizing: content-box ;-->
                    <!--        width: 30px;-->
                    <!--        height: 30px;-->
                    <!--        border-width: 0;-->
                    <!--    //transition: all .3s linear;-->
                    <!--    }-->
                    <!--    .check2ndtype:checked{-->
                    <!--        background-color: #2ECC71;-->
                    <!--    }-->
                    <!--    .check2ndtype:focus{-->
                    <!--        outline: 0 none;-->
                    <!--        box-shadow: none;-->
                    <!--    }-->
                    <!--</style>-->
                </div>
            </div>
        </div>
    </div>
    <div class="popup" style="display:none">
        <div class="header">
            <?php echo $this->Html->image('delete-btn.png', array('alt' => 'CakePHP', 'class' => 'delete')) ?>
            <h1 class="popup-font text-center">You're almost done!</h1>
            <p class="text-center">To complete your booking you need to have an account with us.</p>
            <p class="text-center">You can access your account online or via our App.</p>
        </div>
        <div class="functionality">
        </div>
        <?php //echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
        <div class="col-md-5 col-sm-5">
            <div class="row">
                <h3 class="loginlabel">Login</h3>
                <form class="form-horizontal login" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="loginpassword" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('loginbtn.png', array('alt' => 'CakePHP', 'id'=>'loginimg')) ?>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 text-center">
            <?php echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="row">
                <form class="form-horizontal signUp" role="form">
                    <h3 class="signlabel">Sign Up</h3>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Name</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Mobile No</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="text" class="form-control" id="mobileno" placeholder="Mobile No">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Email</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="email" class="form-control" id="signemail" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="signpassword" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                        <div class="col-sm-8">
                            <input autocomplete="off"  type="password" class="form-control" id="cp" placeholder="Retype Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label label-left"></label>
                        <div class="col-sm-8">
                            <?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'signimg')) ?>
                        </div>
                    </div>
            </div>

            </form>
        </div>
    </div>
</div>

<?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>
