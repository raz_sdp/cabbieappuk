<div class="archivedBookings index">
	<h2><?php echo __('Cancelled Bookings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up'); ?></th>
			<th><?php echo $this->Paginator->sort('destination'); ?></th>
			<th><?php echo $this->Paginator->sort('is_sap_booking', 'SAP Booking'); ?></th>
			<th><?php echo $this->Paginator->sort('at_time'); ?></th>
			<th><?php echo $this->Paginator->sort('persons', 'Passengers'); ?></th>
			<th><?php echo $this->Paginator->sort('fare'); ?></th>
			<th><?php echo $this->Paginator->sort('real_fare'); ?></th>
			<th><?php echo $this->Paginator->sort('is_paid'); ?></th>
			<th><?php echo $this->Paginator->sort('requester'); ?></th>
			<th><?php echo $this->Paginator->sort('acceptor'); ?></th>
			<th><?php echo $this->Paginator->sort('accept_time'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php //print_r($archivedBookings); ?>
	<?php foreach ($archivedBookings as $archivedBooking): ?>
	<?php 
		$sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout','request_sent' => 'Request Sent'); 
	?>
	<tr>
		<td><?php
		if(!empty($archivedBooking['ArchivedBooking']['from_web'])) echo $this->Html->image('/img/web.jpg',array('alt'=>'web'));
		else echo $this->Html->image('/img/app.jpg',array('alt'=>'app'));
		echo h($archivedBooking['ArchivedBooking']['id']); 
		?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination']); ?>&nbsp;</td>
		<td><?php
		//echo h($archivedBooking['ArchivedBooking']['is_sap_booking']); 
		if($archivedBooking['ArchivedBooking']['is_sap_booking'] == '1') echo 'Yes';
		else echo 'No';
		?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['at_time']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['persons']); ?>&nbsp;</td>
		<td><?php echo $this->Number->currency($archivedBooking['ArchivedBooking']['fare'], 'GBP'); ?>&nbsp;</td>
		<td><?php echo $this->Number->currency($archivedBooking['ArchivedBooking']['real_fare'], 'GBP'); ?>&nbsp;</td>
		<td><?php
		if($archivedBooking['ArchivedBooking']['is_paid']=='1'){
				echo 'Yes';
			} else {
				echo 'No';
			}
		?>&nbsp;</td>
		<td><?php
		// echo h($archivedBooking['ArchivedBooking']['requester']); 
		if(!empty($archivedBooking['Requester']['name'])){
			echo $this->Html->link($archivedBooking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $archivedBooking['ArchivedBooking']['requester'])); 
		} else echo 'User has been deleted'
		?>&nbsp;</td>
		<td><?php
		if(!empty($archivedBooking['ArchivedBooking']['acceptor'])) {
				echo $this->Html->link($archivedBooking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $archivedBooking['ArchivedBooking']['acceptor'])); 
			} else {
				if(empty($archivedBooking['ArchivedBooking']['status'])) echo 'Queued';
				else {
					$val = $archivedBooking['ArchivedBooking']['status'];
					echo $sts[$val]; 
				}
			}
		?>&nbsp;</td>
		<td><?php
		echo h($archivedBooking['ArchivedBooking']['accept_time']); 
			if (!empty($archivedBooking['ArchivedBooking']['accept_time'])) {
				$date_time = date_create($archivedBooking['ArchivedBooking']['accept_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
					echo 'N/A';
			}
		?>&nbsp;</td>
		<td><?php
		$date_time = date_create($archivedBooking['ArchivedBooking']['created']);
				echo date_format($date_time,'d/m/Y g:i A');
		?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $archivedBooking['ArchivedBooking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>
