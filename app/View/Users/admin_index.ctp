<?php
	$types = array(
				'driver' => 'Driver',
				'passenger' => 'Passenger',
				'admin'	=> 'Admin'
				
			);
?>
<div class="users index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add new '. $types[$type], array('controller' => 'users', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $type))?></div>
	<h2><?php echo __($types[$type]); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile'); ?></th>
			<th><?php echo $this->Paginator->sort('home_no'); ?></th>
		<?php if($type!= 'passenger') { ?>
			<th><?php echo $this->Paginator->sort('promoter_code'); ?></th>
		<?php } ?>	
		<?php if($type == 'driver') { ?>
			<th><?php echo $this->Paginator->sort('ref_promoter_code','Reference Promoter Code'); ?></th>
			<th><?php echo $this->Paginator->sort('running_distance','Running Distance (miles)'); ?></th>
			<th><?php echo $this->Paginator->sort('voip_no','Phone No for Phonecall zone(VOIP No)'); ?></th>
		<?php } ?>		
			<th> <?php if($type == 'driver') { ?>
				<?php echo $this->Paginator->sort('is_enabled','Enabled'); 
				 } ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['address']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['home_no']); ?>&nbsp;</td>
	<?php if($type!= 'passenger') { ?>
		<td><?php echo h($user['User']['promoter_code']); ?>&nbsp;</td>
	<?php } ?>
	<?php if($type == 'driver') { ?>
		<td><?php echo h($user['User']['ref_promoter_code']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['running_distance']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['voip_no']); ?>&nbsp;</td>
	<?php } ?>
		<td>
			<?php if($type == 'driver'){
				if(!empty($user['User']['is_enabled'])) {
					echo h('Yes'); 
				} else {
					echo h('No');
				}
			}
			?>
			&nbsp;
		</td>
		<td class="actions">
			<?php 
			if ($type == 'driver') {
				if($user['User']['is_enabled'] == 0){
					echo $this->Html->link(__('Enable'), array( 'action' => 'enable_driver', $user['User']['id']), array('div' => false, 'class' => 'e_d')); 
				} else {
					echo $this->Html->link(__('Disable'), array('action' => 'disable_driver', $user['User']['id']), array('div' => false, 'class' => 'd_d')); 
				}
			}
			?>
			<?php echo $this->Html->link(__('Give Free Credit'), array('action' => 'freetoup', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'admin_delete', $user['User']['id']), null, __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>