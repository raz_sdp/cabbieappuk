<div class="fareSettings index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add Fare by Miles', array('controller' => 'fare_settings', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></div>
	<h2><?php echo __('Fare By Miles'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('zone_id','Zone');?></th>
		<th><?php echo $this->Paginator->sort('distance','Miles (Up To)'); ?></th>
		<th><?php echo $this->Paginator->sort('holidaytype_id','Holiday Type'); ?></th>
		<th><?php echo $this->Paginator->sort('weeks','Weekdays'); ?></th>
		<th><?php echo $this->Paginator->sort('time_from', 'Start Time'); ?></th>
		<th><?php echo $this->Paginator->sort('time_to', 'End Time'); ?></th>
		<th><?php echo $this->Paginator->sort('s_4','Base Fare'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($fareSettings as $fareSetting): ?>
	<?php if($fareSetting['FareSetting']['parent'] == null) { ?>
	<tr>
	<?php if($fareSetting['FareSetting']['zone_id'] == null) { ?>
	<td>Default</td>
	<?php } else { ?>
		<td><?php echo h($fareSetting['Zone']['name']); ?> </td>
	<?php } ?>
	<td><?php echo h($fareSetting['FareSetting']['distance']);?>&nbsp;</td>
	<td><?php if($fareSetting['Holidaytype']['holiday_type'] != Null) { 
        echo h($fareSetting['Holidaytype']['holiday_type']); 
    	} else echo h('N/A'); 
        ?></td>
        <?php if($fareSetting['FareSetting']['weeks'] != Null) { 
   	    	$weekdays =  array('0' => 'Sunday', '1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
			$day = trim($fareSetting['FareSetting']['weeks'], '#');
			$wks = explode('#', $day);
		?>
	        <td><?php foreach ($wks as $key) {
				echo $weekdays [$key];?>
				<br><?php
			} ?>&nbsp;</td>
		<?php } else { ?>
		 		<td><?php echo ('N/A'); ?>&nbsp;</td>
		<?php } 
			$c_date = date_create($fareSetting['FareSetting']['created']);
			$s_time = date_create($fareSetting['FareSetting']['time_from']);
			$e_time = date_create($fareSetting['FareSetting']['time_to']);
		?>
		<td><?php echo date_format($s_time, 'g:i A'); ?>&nbsp;</td>
		<td><?php echo date_format($e_time, 'g:i A'); ?>&nbsp;</td>
		<td><?php echo $this->Number->currency($fareSetting['FareSetting']['s_4'], 'GBP'); ?>&nbsp;</td>
		<td><?php echo date_format($c_date,'d/m/Y'); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $fareSetting['FareSetting']['id'])); ?>
			<?php if(!empty($fareSetting['Zone']['name'])){
			echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $fareSetting['FareSetting']['id']), null, __('Are you sure you want to delete  %s?', $fareSetting['Zone']['name']));
			} else {
				echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $fareSetting['FareSetting']['id']), null, __('Are you sure you want to delete Default?')); 
			} ?>
		</td>
	</tr>
	<?php } ?>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>