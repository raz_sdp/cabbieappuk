<style>
	form div.checkbox{
		float: left;
		clear: none;
	}
</style>
<?php
	echo $this->Html->script(array('add_fare.js?10'));
	echo $this->Html->script('fare_holidays', array('inline'=>false));

?>
<div class="fareSettings form">
<?php echo $this->Form->create('FareSetting'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Fare By Miles'); ?></legend>
	<?php
		echo $this->Form->input('zone_id', array('empty' => 'Default'));
		echo $this->Form->input('note', array('label' => 'Note For Passenger & Driver'));
		echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type','empty' => '(Not A Holiday)'));
		$weekdays =  array('0' => 'Sunday', '1'=> 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
       	echo $this->Form->input('weeks', array('label' => false ,'multiple' => 'checkbox', 'options' => $weekdays, 'div' => false));
		echo $this->Form->input('time_from', array('label' => 'Start Time'));
		echo $this->Form->input('time_to', array('label' => 'End Time'));
		echo $this->Form->input('fare_setting_type',array('value' => 'fare_by_miles', 'type'=>'hidden'));
		?><div class='AddMoreClass'><?php
		?>Mile Up To <?php
		echo $this->Form->input('distance',array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][distance][]','div' => false));
		?>&nbsp;&nbsp;<span class='fareClass'>Base Fare</span> <?php
		echo $this->Form->input('s_4', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_4][]', 'div' => false));
		?><br><br><label><strong>Standard Car</strong></label>5 Passengers <?php
		echo $this->Form->input('s_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_5][]', 'div' => false));
		?>&nbsp;&nbsp;6 Passengers <?php
		echo $this->Form->input('s_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_6][]', 'div' => false));
		?>&nbsp;&nbsp;7 Passengers <?php
		echo $this->Form->input('s_7', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_7][]', 'div' => false));
		?>&nbsp;&nbsp;8 Passengers <?php
		echo $this->Form->input('s_8', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][s_8][]', 'div' => false));
		?><br><br><label><strong>Estate Car</strong></label><?php
		?>4 Passengers <?php
		echo $this->Form->input('e_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_4][]', 'div' => false));
		?>&nbsp;&nbsp;5 Passengers <?php
		echo $this->Form->input('e_5', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_5][]', 'div' => false));
		?>&nbsp;&nbsp;6 Passengers <?php
		echo $this->Form->input('e_6', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_6][]', 'div' => false));
		?>&nbsp;&nbsp;7 Passengers <?php
		echo $this->Form->input('e_7', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_7][]', 'div' => false));
		?>&nbsp;&nbsp;8 Passengers <?php
		echo $this->Form->input('e_8', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][e_8][]', 'div' => false));
		?><br><br><label><strong>Wheelchair</strong></label>
		3 Passengers <?php
		echo $this->Form->input('w_3', array('label' => false,'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_3][]','div' => false));
		?>&nbsp;&nbsp;4 Passengers <?php
		echo $this->Form->input('w_4', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_4][]','div' => false));
		?>&nbsp;&nbsp;5 Passengers <?php
		echo $this->Form->input('w_5', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_5][]', 'div' => false));
		?>&nbsp;&nbsp;6 Passengers <?php
		echo $this->Form->input('w_6', array('label' => false, 'style' => 'width:100px; height:30px; padding:0', 'id' => false, 'name' => 'data[FareSetting][w_6][]', 'div' => false));
		?></div><?php
		echo $this->Form->input('Add More',array('type' => 'button', 'label' => false, 'value' =>'Add More', 'id' => 'AddMore'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
