<div class="fareSettings view">
<h2><?php echo __('Fare Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('From Postcode'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['from_postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('From Airport'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['from_airport']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('To Postcode'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['to_postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('To Airport'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['to_airport']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Distance'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['distance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weeks'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['weeks']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time From'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['time_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time To'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['time_to']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S 4'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['s_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S 5'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['s_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S 6'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['s_6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S 7'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['s_7']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S 8'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['s_8']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('E 4'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['e_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('E 5'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['e_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('E 6'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['e_6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('E 7'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['e_7']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('E 8'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['e_8']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('W 3'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['w_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('W 4'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['w_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('W 5'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['w_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('W 6'); ?></dt>
		<dd>
			<?php echo h($fareSetting['FareSetting']['w_6']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fare Setting'), array('action' => 'edit', $fareSetting['FareSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fare Setting'), array('action' => 'delete', $fareSetting['FareSetting']['id']), null, __('Are you sure you want to delete # %s?', $fareSetting['FareSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fare Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fare Setting'), array('action' => 'add')); ?> </li>
	</ul>
</div>
