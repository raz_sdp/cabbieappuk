<div class="queues view">
<h2><?php  echo __('Queue'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($queue['Queue']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Virtual Rank'); ?></dt>
		<dd>
			<?php echo $this->Html->link($queue['VirtualRank']['id'], array('controller' => 'virtual_ranks', 'action' => 'view', $queue['VirtualRank']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($queue['User']['name'], array('controller' => 'users', 'action' => 'view', $queue['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Position'); ?></dt>
		<dd>
			<?php echo h($queue['Queue']['position']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($queue['Queue']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($queue['Queue']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Queue'), array('action' => 'edit', $queue['Queue']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Queue'), array('action' => 'delete', $queue['Queue']['id']), null, __('Are you sure you want to delete # %s?', $queue['Queue']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Queues'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Queue'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Virtual Ranks'), array('controller' => 'virtual_ranks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Virtual Rank'), array('controller' => 'virtual_ranks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
