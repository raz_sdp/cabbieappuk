<div class="queues form">
<?php echo $this->Form->create('Queue'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Queue'); ?></legend>
	<?php
		echo $this->Form->input('zone_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('type');
		echo $this->Form->input('position');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
