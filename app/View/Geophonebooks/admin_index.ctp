<div class="geophonebooks index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add new Geo Phone Book', array('controller' => 'geophonebooks', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></div>

	<h2><?php echo __('Geophonebooks'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('phone','Phone Number'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($geophonebooks as $geophonebook): ?>
	<tr>
		<td><?php echo h($geophonebook['Geophonebook']['address']); ?>&nbsp;</td>
		
		<td><?php echo h($geophonebook['Geophonebook']['phone']); ?>&nbsp;</td>
		<td class="actions">
			
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $geophonebook['Geophonebook']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $geophonebook['Geophonebook']['id']), null, __('Are you sure you want to delete %s?', $geophonebook['Geophonebook']['address'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>
