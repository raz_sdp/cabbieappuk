<?php
App::uses('AppController', 'Controller');
/**
* Users Controller
*
* @property User $User
*/
class UsersController extends AppController {

	//public $components = array('RequestHandler');
	
	/**
	* admin_index method
	*
	* @return void
	*/
	public function admin_index($type = 'driver') {
		//$this->loadModel('Zone');
		$this->User->recursive = -1;
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('User.type' => $type),
            'order' => array('User.name' => 'ASC'),
	    );
	    $users = $this->paginate();
		$this->set(compact('users','type'));
	}
	
	/**
	* admin_view method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_view($id = null, $type = 'driver') {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));

	}
	
	private function _voip_check($number){
		$pos = stripos($number, '+44'); 
		if($pos === false) {
			$number = preg_replace('/^0/', '+44', $number, 1); 
		}
		return $number;
	}
	/**
	* admin_add method
	*
	* @return void
	*/
	public function admin_add($type = 'driver') {
		//print_r($this->Auth->user());
//		Configure::write('debug', 2);
		if ($this->request->is('post')) {
			$email_exist = $this->User->findByEmail($this->request->data['User']['email'], array('id'));
			if(empty($email_exist)) {
				$mobile_exist = $this->User->findByMobile($this->request->data['User']['mobile'], array('id'));
				if(empty($mobile_exist)){
					$this->User->create();
					$code = String::uuid();
					$this->request->data['User']['code'] = $code;
					$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
					if($this->request->data['User']['type'] != 'passenger') {
						$my_promoter_code = $this->User->create_rand_promoter_code();
						$this->request->data['User']['promoter_code'] = $my_promoter_code;
					}
					if($this->request->data['User']['type'] == 'driver') {
						$this->request->data['User']['is_enabled'] = 0;
						$this->request->data['User']['insurance_certificate'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['User']['insurance_certificate'])));
					}
					$image_upload_user = $this->_upload($this->request->data['User']['imagex']);
					if (!empty($image_upload_user)) {
						$this->request->data['User']['image'] = $image_upload_user;
					}
					
					//print_r($this->request->data); exit;

					$image_upload_badge = $this->_upload($this->request->data['User']['badge_imagex']);
					if (!empty($image_upload_badge)) {
						$this->request->data['User']['badge_image'] = $image_upload_badge;
					}
					$image_upload_insurance = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);
					if (!empty($image_upload_insurance)) {
						$this->request->data['User']['insurance_certificate_image'] = $image_upload_insurance;
					}
					$image_upload_vehicle = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
					if (!empty($image_upload_vehicle)) {
						$this->request->data['User']['vehicle_licence_image'] = $image_upload_vehicle;
					}
					$this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);
					if ($this->User->save($this->request->data)) {
						$this->loadModel('Setting');
							$data_setting = $this->Setting->findById(1);	
							$id = $this->User->id;
							if($this->request->data['User']['type'] == 'driver') 
							{	
								$this->_new_driver_in_queue($id);
								$balance = $data_setting['Setting']['free_credit_driver'];
								$this->_add_free_credit($id, $balance); // free credit for new sign up 
							}
							else if ($this->request->data['User']['type'] == 'passenger')  {
								$balance = $data_setting['Setting']['free_credit_passenger'];
								$this->_add_free_credit($id, $balance); // free credit for new sign up 
							}
						$this->Session->setFlash(__('The user has been saved'));
						$this->redirect(array('action' => 'index', $type));
					} else {
						$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
					}
				} else $this->Session->setFlash(__('The Duplicate Mobile No. Please, Try Another.'));
			} else $this->Session->setFlash(__('The Duplicate Email. Please, Try Another.'));
		}
		$ref_promoters = $this->User->find('list', array('conditions' => array('User.type' => 'admin'), 'fields' => array('User.promoter_code')));
		$ref_prom = array();
		foreach ($ref_promoters as $ref_promoter) {
			$ref_prom[$ref_promoter] = $ref_promoter;
		}
		$this->loadModel('Zone');
		$vr_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'vr_zone')));
		$phonecall_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'phonecall_zone')));
		$phonecall_gps_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'phonecall_gps_zone')));
		$this->set(compact('vr_zones','phonecall_zones','phonecall_gps_zones','type','ref_prom'));
	}
	

	/**
	* admin_edit method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$email_exist = $this->User->find('first', array(
					'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.id <>'=> $id,'User.type' =>$this->request->data['User']['type']),
					'fields' => array('User.id'),
				));
			//print_r($email_exist); exit;
			if(empty($email_exist)) {
				$mobile_exist = $this->User->find('first', array(
					'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.id <>'=> $id,'User.type' =>$this->request->data['User']['type']),
					'fields' => array('User.id'),
				));
				if(empty($mobile_exist)){
					if(empty($this->request->data['User']['passwordx'])){
						unset($this->request->data['User']['passwordx']);
					} else {
						$this->request->data['User']['password'] =  AuthComponent::password($this->request->data['User']['passwordx']);
					}
					$this->request->data['User']['insurance_certificate'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['User']['insurance_certificate'])));
					$image_upload_user = $this->_upload($this->request->data['User']['imagex']);
					if (!empty($image_upload_user)) {
						$this->request->data['User']['image'] = $image_upload_user;
					}
					$image_upload_badge = $this->_upload($this->request->data['User']['badge_imagex']);
					if (!empty($image_upload_badge)) {
						$this->request->data['User']['badge_image'] = $image_upload_badge;
					}
					$image_upload_insurance = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);
					if (!empty($image_upload_insurance)) {
						$this->request->data['User']['insurance_certificate_image'] = $image_upload_insurance;
					}
					$image_upload_vehicle = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
					if (!empty($image_upload_vehicle)) {
						$this->request->data['User']['vehicle_licence_image'] = $image_upload_vehicle;
					}
					$this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);
					if ($this->User->save($this->request->data)) {
						$this->Session->setFlash(__('The user has been saved'));
						$this->redirect(array('action' => 'index', $this->request->data['User']['type']));
					} else {
						$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
						$this->redirect(array('action' => 'index', $this->request->data['User']['type']));
					}
				} else $this->Session->setFlash(__('The Duplicate Mobile. Please, Try Another.'));

			} else $this->Session->setFlash(__('The Duplicate Email. Please, Try Another.'));
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		// /print_r($this->request->data);
		$this->loadModel('Zone');
		$this->recursive = -1;
		$vr_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'vr_zone')));
		$phonecall_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'phonecall_zone')));
		$phonecall_gps_zones = $this->Zone->find('list',array('conditions' => array('Zone.type' => 'phonecall_gps_zone')));
		$this->set(compact('vr_zones','phonecall_zones','phonecall_gps_zones'));
	}
	
	/**
	* admin_delete method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			//$this->loadModel('Transaction');
			$this->User->query('DELETE FROM transactions WHERE user_id = \'' . $id . ' \'
			');
			//$this->loadModel('Queue');
			$this->User->query('DELETE FROM queues WHERE user_id = \'' . $id . ' \'
			');
			//$this->loadModel('PhoneQueue');
			$this->User->query('DELETE FROM phone_queues WHERE user_id = \'' . $id . ' \'
			');
			//$this->loadModel('DeviceToken');
			$this->User->query('DELETE FROM device_tokens WHERE user_id = \'' . $id . ' \'
			');
			$this->Session->setFlash(__('User deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index', $this->request->data['User']['type']));
	}
	

	// function to set is_deleted = 1
	/*public function admin_is_deleted($id = null) {
		$this->autoRender = false;
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		$user = $this->User->findById($id);
		$this->request->data['User']['is_deleted'] = 1;		
		if ($this->User->save($this->request->data, false)) {
			$this->Session->setFlash(__('The user is deleted.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('User was not deleted'));
		}
	//	return $this->redirect(array('action' => 'index'));


	//	$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index', $this->request->data['User']['type']));
	}
	*/

	//admin login
	public function admin_login(){
		if($this->request->is('post')){
			//print_r(AuthComponent::password($this->request->data['User']['password']));exit;
			//print_r($this->request->data); exit;
			if($this->Auth->login() && $this->Auth->User('type') == 'admin'){
				//print_r($this->Auth->user()); exit;
				$this->redirect($this->Auth->redirect());
			}else{
				$this->Auth->logout();
				$this->Session->setFlash(__('Invalid user. You have no access here.'));
			}
		}
	}
	
	//admin logout
	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}
	
	// passenger login
	public function passenger_login(){
		$this->request->data['User']['type'] = 'passenger';
		$this->_login();
	}
	// driver login
	public function driver_login(){
		$this->request->data['User']['type'] = 'driver';
		$this->_login();
	}

	public function login(){
	}

	public function logout() {
		$this->redirect($this->Auth->logout());
	}
	//users login
	private function _login(){
		$this->autoRender = false;
		if($this->request->is('post')){
			$this->request->data['User']['passwordx'] = AuthComponent::password($this->request->data['User']['password']);
			$time_pre = microtime(true);
			$user = $this->User->find('first',array(
				'conditions' => array(
					'OR' => array(
						'User.email' => $this->request->data['User']['login'],
						'User.mobile' => $this->request->data['User']['login']
					),
					'User.password' => $this->request->data['User']['passwordx'],
					'User.type' => $this->request->data['User']['type']
				),
				//'recursive' => -1
			));
			if(!empty($user)){
				$this->request->data['User']['email'] = $this->request->data['User']['login'];
				//static user code
				if(empty($user['User']['code'])){
					$user['User']['code'] = String::uuid();
					$this->User->id = $user['User']['id'];
					$this->User->saveField('code', $user['User']['code']);	
				}
				if(!empty($this->request->data['User']['device_token'])){
					if(empty($this->request->data['User']['device_type'])){
						$this->request->data['User']['device_type'] = 'android';
					}
					$this->_device_token(
						$user['User']['id'], 
						$this->request->data['User']['device_token'],
						$this->request->data['User']['device_type'], 
						$this->request->data['Application']['stage'],
						$this->request->data['User']['old_device_token']
					);
				}
				
				//device token array 
				$devices = array();
				if(!empty($user['DeviceToken'])){
					foreach ($user['DeviceToken'] as $device_tokens) {
						$devices[] = $device_tokens['device_token'];
					}
				}

				$this->User->id = $user['User']['id'];
				$this->User->saveField('last_activity', date('Y-m-d H:i:s'));

				//$this->User->saveField('device_token', $this->request->data['User']['device_token']);
				if ($user['User']['type'] == 'driver')
				{
					//checks for the zones in where diver is permitted
					$isdav = $this->isdav($user['User']['code'], 'all', 'yes', true);
					echo json_encode(array(
						'success' => true, 
						'User' => array(
							'id' => $user['User']['id'], 
							'code' => $user['User']['code'],
							'name' => $user['User']['name'],
							'promoter_code' => $user['User']['promoter_code'], 
							'ref_promoter_code' => $user['User']['ref_promoter_code'], 
							'email' => $user['User']['email'], 
							'mobile' => $user['User']['mobile'], 
							'address' => $user['User']['address'], 
							'home_no' => $user['User']['home_no'], 
							'cab_type' => $user['User']['cab_type'], 
							'vehicle_type' => $user['User']['vehicle_type'], 
							'is_wheelchair' => $user['User']['is_wheelchair'],
							'vehicle_licence_no' => $user['User']['vehicle_licence_no'], 
							'vehicle_licence_image' => $user['User']['vehicle_licence_image'], 
							'image' => $user['User']['image'], 
							'badge_no' => $user['User']['badge_no'], 
							'badge_image' => $user['User']['badge_image'], 
							'registration_plate_no' => $user['User']['registration_plate_no'], 
							'insurance_certificate' => $user['User']['insurance_certificate'], 
							'insurance_certificate_image' => $user['User']['insurance_certificate_image'], 
							'type' => $user['User']['type'], 
							'device_token' => $devices, 
							'no_of_seat' => $user['User']['no_of_seat'],
							'voip_no' => $user['User']['voip_no'],
							'running_distance' => $user['User']['running_distance'],
							'is_enabled' => $user['User']['is_enabled'],
							'account_holder_name' => $user['User']['account_holder_name'],
							'account_no' => $user['User']['account_no'],
							'sort_code' => $user['User']['sort_code'],
							'iban' => $usre['User']['iban'],
							'swift' => $user['User']['swift']
						),
						'isdav' => array(
							'success' => true,
							'vr' => false,
							'vr_position' => null,
							'vr_zone_id' => null,
							'vr_zone' => null,
							'vr_btn' => 'N/A',
							'phone_gps' => false,
							'phone_gps_position' => null,
							'phone_gps_zone_id' => null,
							'phone_gps_zone' => null,
							'phone_gps_btn' => 'N/A',
							'phone' => false,
							'phone_position' => null,
							'phone_zone_id' => null,
							'phone_zone' => null,
							'phone_btn' => 'N/A' ,
							'msg'=>''
							)
					));
				} else {
					// for log in via brower
					echo json_encode(array(
						'success' => true,
						'User' => array(
							'id' => $user['User']['id'], 
							'code' => $user['User']['code'], 
							'name' => $user['User']['name'], 
							'email' => $user['User']['email'], 
							'mobile' => $user['User']['mobile'], 
							'address' => $user['User']['address'], 
							'home_no' => $user['User']['home_no'], 
							'type' => $user['User']['type'], 
							'device_token' => $devices
						)
					));
				}
				$response = $user['User']['id'];
				// API log	
					$this->_apiLog(
		          	$user_id, 
		          	var_export($this->request->data, true), 
		          	$response, 
		          	1, 
		          	$time_pre);

		         // for log in via brower
				 $this->Auth->login(); 
			}else{
				echo json_encode(array(
					'success' => false, 
					'message' => 'Not valid email or password'
				));
			}
		}
	}
	
	public function user_logout(){
		$this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('put')) {			
			$code = $this->request->data['User']['code'];
			$user = $this->User->findByCode($code);
				$this->request->data['User']['id'] = $user['User']['id'];
				$this->request->data['User']['phonecall_available'] = 'no';
				$this->request->data['User']['phonecall_gps_available'] = 'no';
				$this->request->data['User']['vr_available'] = 'no';
				if ($this->User->save($this->request->data, false)) {
					die(json_encode(array('success' => true, 'message' => 'You\'ve been logout')));
				} else {
					die(json_encode(array('success' => true, 'message' => 'Action failed')));
				}
		} else die(json_encode(array('success' => false, 'message' => 'Invalid request')));
	}
	
	public function signup(){

	}

	//driver/passenger registration
	public function register(){
		header("Content-Type: application/json;charset=utf-8");
//		Configure::write('debug', 2);
		$this->autoRender = false;
        $this->User->recursive = -1;
		if($this->request->is('post') || $this->request->is('put') && !empty($this->request->data)){
			if($this->request->data['User']['type'] == 'driver'){
				$email_check = $this->User->find('first', array(
					'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.type' => 'driver')
				));
				$mobile_check = $this->User->find('first', array(
					'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.type' => 'driver')
				));
				if(empty($email_check) && empty($mobile_check)){
					/*
					* Check for promoter code validity for driver
					*/
					$is_valid_promoter_code = $this->User->check_valid_promoter_code(trim($this->request->data['User']['ref_promoter_code']));
					if(!$is_valid_promoter_code) {
						die(json_encode(array('success' => false, 'message' => __('Not a valid promoter code.'))));
					} else {
						// set promoter code
						$my_promoter_code = $this->User->create_rand_promoter_code();
						$this->request->data['User']['promoter_code'] = $my_promoter_code;
						
						// set is_enabled = 0 so that new driver can not log-in without admin's permission, by default every passenger is enabled
						$this->request->data['User']['is_enabled'] = 0;
						$this->_save_user($my_promoter_code);
					}
				} elseif(!empty($email_check)) {
					die(json_encode(array('success' => false, 'message' => 'Email already exists.')));	
				} else die(json_encode(array('success' => false, 'message' => 'Mobile already exists.')));
				
			} elseif($this->request->data['User']['type'] == 'passenger'){
				$email_check = $this->User->find('first', array(
					'conditions' => array('User.email' => $this->request->data['User']['email'], 'User.type' => 'passenger')
				));
				$mobile_check = $this->User->find('first', array(
					'conditions' => array('User.mobile' => $this->request->data['User']['mobile'], 'User.type' => 'passenger')
				));
				
				if(empty($email_check) && empty($mobile_check)){
					$this->_save_user();
				} elseif(!empty($email_check)) {
					die(json_encode(array('success' => false, 'message' => 'Email already exists.')));	
				} else die(json_encode(array('success' => false, 'message' => 'Mobile already exists.')));
			}
		} else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}		

	public function sendto_passenger(){
		$this->autoRender = false;
		//$this->_sendSms($this->request->data['User']['mobile'], 'CabbieAppUK! - Account created successfully'); 
		try{
			$this->_sendEmail(
				$this->request->data['User']['email'],
				"Welcome to CabbieAppUK!" ,
				"Dear ".$this->request->data['User']['name'].",<br/>Your account has been created succesfully<br/><br/>Regards,<br/><br/>The CabbieApp Team",
				'welcome',
				array('title' => 'Thanks for registering with CabbieAppUK!')
			);
		} catch(Exception $e){

		}
	}
	


	// new driver in queue
	private function _new_driver_in_queue($id) {
		$this->loadModel('Queue');
		$data['Queue']['user_id'] = $id;
		$data['Queue']['booking_id'] = null;
		$now = date("Y-m-d H:i:s");
		$data['Queue']['request_time'] = $now;
		$this->Queue->create();
		$this->Queue->save($data, false);
		$this->loadModel('PhoneQueue');
		$phonedata['PhoneQueue']['user_id'] = $id;
		$phonedata['PhoneQueue']['request_time'] = $now;
		$this->PhoneQueue->create();
		$this->PhoneQueue->save($phonedata, false);

	}


	//free credit for new passengers/drivers
	private function _add_free_credit($id, $balance) {
		$this->loadModel('Transaction');
		$data['Transaction']['user_id'] = $id;
		$data['Transaction']['amount'] = $balance;
		$data['Transaction']['amount_after'] = $balance;
		$data['Transaction']['short_description'] = 'free_credit';
		$data['Transaction']['service'] = 'free_credit';
		$now = date('Y-m-d');
		$data['Transaction']['timestamp'] = $now;
		$this->Transaction->create();
		$this->Transaction->save($data, false);
	}


	//edit driver/passenger via app
	public function update($code=null){
		$this->autoRender = false;
		$user = $this->User->findByCode($code);
		//print_r($user); exit;
		$time_pre = microtime(true);
		$this->User->recursive = -1;
		$email_check = $this->User->find('all', array(
				'conditions' => array('User.id <>' => $user['User']['id'], 
					'User.email' => $this->request->data['User']['email'], 
					'User.type' => $user['User']['type']),
				'fields' => array('User.id'),
			));
		$mobile_check = $this->User->find('all', array(
				'conditions' => array('User.id <>' => $user['User']['id'], 
					'User.mobile' => $this->request->data['User']['mobile'],
					'User.type' => $user['User']['type']),
				'fields' => array('User.id'),
			));
		if(($this->request->is('post') || $this->request->is('put')) && !empty($this->request->data) && !empty($user) && empty($email_check) && empty($mobile_check)) {
			//print_r($this->request->data);
			$user_type = $user['User']['type'];
			$user_id =  $user['User']['id'];
			if(empty($this->request->data['User']['password'])){
				unset($this->request->data['User']['password']);
			} else {
				$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
			}
			if (empty($this->request->data['User']['voip_no'])) {
				unset($this->request->data['User']['voip_no']);
			}
			if($user_type == 'driver') {		

				if(!empty($this->request->data['User']['imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['imagex']['tmp_name'])){
					$this->request->data['User']['image'] = $this->_upload($this->request->data['User']['imagex']);
				}

				if(!empty($this->request->data['User']['badge_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['badge_imagex']['tmp_name'])){
					$this->request->data['User']['badge_image'] = $this->_upload($this->request->data['User']['badge_imagex']);					
				}
				if(!empty($this->request->data['User']['insurance_certificate_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_certificate_imagex']['tmp_name'])){
					$this->request->data['User']['insurance_certificate_image'] = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);
					
				}
				if(!empty($this->request->data['User']['vehicle_licence_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['vehicle_licence_imagex']['tmp_name'])){
					$this->request->data['User']['vehicle_licence_image'] = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
				}

                /*In v2 added this */
                if(!empty($this->request->data['User']['drivers_licence_imagex_front']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_front']['tmp_name'])){
                    $this->request->data['User']['drivers_licence_image_front'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_front']);
                }
                if(!empty($this->request->data['User']['drivers_licence_imagex_back']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_back']['tmp_name'])){
                    $this->request->data['User']['drivers_licence_image_back'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_back']);
                }
                /*if(!empty($this->request->data['User']['insurance_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_imagex']['tmp_name'])){
                    $this->request->data['User']['insurance_image'] = $this->_upload($this->request->data['User']['insurance_imagex']);
                }*/

				//$this->request->data['User']['voip_no'] = $this->_voip_check($this->request->data['User']['voip_no']);
				if($user['User']['is_enabled'] == 0) {
					$this->request->data['User']['first_update'] = 1;
				}
				$this->User->id = $user_id;
				$this->User->save($this->request->data, false);
				$userInfo = am($user['User'], $this->request->data['User']);
				
				//device token array 
				$devices = array();
				if(!empty($user['DeviceToken'])){
					foreach ($user['DeviceToken'] as $device_tokens) {
						$devices[] = $device_tokens['device_token'];
					}
				}

				if($user['User']['is_enabled'] == 0) {
					$msg = 'Please wait. You will receive a e-mail notification you have been activated';
					try{
						$this->_activateEmail($user_id, true);
					} catch (Exception $e){

					}
				} else {
					$msg = 'Update Successful';
				}

				$response = json_encode(array(
					'success' => true,
					'msg' => $msg, 
					'User' => array(
						'id' => $userInfo['id'], 
						'code' => $userInfo['code'],
						'name' => $userInfo['name'],
						'promoter_code' => $userInfo['promoter_code'], 
						'ref_promoter_code' => $userInfo['ref_promoter_code'], 
						'email' => $userInfo['email'], 
						'mobile' => $userInfo['mobile'], 
						'address' => $userInfo['address'], 
						'home_no' => $userInfo['home_no'], 
						'cab_type' => $userInfo['cab_type'], 
						'vehicle_type' => $userInfo['vehicle_type'], 
						'is_wheelchair' => $userInfo['is_wheelchair'], 
						'vehicle_licence_no' => $userInfo['vehicle_licence_no'], 
						'vehicle_licence_image' => $userInfo['vehicle_licence_image'], 
						'image' => $userInfo['image'], 
						'badge_no' => $userInfo['badge_no'], 
						'badge_image' => $userInfo['badge_image'], 
						'registration_plate_no' => $userInfo['registration_plate_no'], 
						'insurance_certificate' => $userInfo['insurance_certificate'], 
						'insurance_certificate_image' => $userInfo['insurance_certificate_image'], 
						'type' => $userInfo['type'], 
						'device_token' => $devices, 
						'no_of_seat' => $userInfo['no_of_seat'],
						'voip_no' => $userInfo['voip_no'],

						'branch_number' => $userInfo['branch_number'],
						'driving_licence_no' => $userInfo['driving_licence_no'],
						'drivers_licence_image_front' => $userInfo['drivers_licence_image_front'],
						'drivers_licence_image_back' => $userInfo['drivers_licence_image_back'],
						'driving_licence_dateExpiry' => $userInfo['driving_licence_dateExpiry'],

						'running_distance' => $userInfo['running_distance'])
				));

				// API log	
				$this->_apiLog(
		        $user_id, 
		        var_export($this->request->data, true), 
		        $response, 
		        1, 
		        $time_pre);

			die($response);
				//echo json_encode(array('success' => true, 'User' => array('id' => $userInfo['id'], 'code' => $userInfo['code'],'name' => $userInfo['name'],'promoter_code' => $userInfo['promoter_code'], 'ref_promoter_code' => $userInfo['ref_promoter_code'], 'email' => $userInfo['email'], 'mobile' => $userInfo['mobile'], 'address' => $userInfo['address'], 'home_no' => $userInfo['home_no'], 'cab_type' => $userInfo['cab_type'], 'vehicle_type' => $userInfo['vehicle_type'], 'is_wheelchair' => $userInfo['is_wheelchair'], 'vehicle_licence_no' => $userInfo['vehicle_licence_no'], 'vehicle_licence_image' => $userInfo['vehicle_licence_image'], 'image' => $userInfo['image'], 'badge_no' => $userInfo['badge_no'], 'badge_image' => $userInfo['badge_image'], 'registration_plate_no' => $userInfo['registration_plate_no'], 'insurance_certificate' => $userInfo['insurance_certificate'], 'insurance_certificate_image' => $userInfo['insurance_certificate_image'], 'type' => $userInfo['type'], 'device_token' => $userInfo['device_token'], 'no_of_seat' => $userInfo['no_of_seat'],'voip_no' => $userInfo['voip_no'],'running_distance' => $userInfo['running_distance']) ));
			} else {
				$this->User->id = $user_id;
				$this->User->save($this->request->data, false);
				$userInfo = am($user['User'], $this->request->data['User']);
				echo json_encode(array(
					'success' => true, 
					'User' => array(
						'id' => $userInfo['id'], 
						'code' => $userInfo['code'], 
						'name' => $userInfo['name'], 
						'email' => $userInfo['email'], 
						'mobile' => $userInfo['mobile'], 
						'address' => $userInfo['address'], 
						'home_no' => $userInfo['home_no'], 
						'type' => $userInfo['type'], 
						'device_token' => $devices)
				));
			}
		} elseif(!empty($email_check)) {
				echo json_encode(array(
					'success' => false, 
					'msg' => 'Duplicate Email.',
				));
		} else {
			echo json_encode(array(
					'success' => false, 
					'msg' => 'Duplicate Mobile.',
				));
		}
		exit;
	}
	
	//forgot password email
	public function iforgot(){
		$this->autoRender = false;
		$this->User->recursive= -1;
		$login = $this->request->data['User']['login'];
		if($this->request->is('post')){
			$user = $this->User->findByEmailOrMobile($this->request->data['User']['login'], $this->request->data['User']['login'], array('User.id', 'User.email'));
			//print_r($user); exit;
			if(!empty($user)){
				$forgot_pwd_token = String::uuid();

				$this->User->id = $user['User']['id'];
				$this->User->saveField('forgot_pwd_token', $forgot_pwd_token);
				
				App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail('smtp');
				$Email->viewVars(array('forgot_pwd_token' =>  $forgot_pwd_token));
				$Email->template('iforgot');
				$Email->to($user['User']['email']);
				$Email->subject('Reset password');
				$Email->send();
				echo json_encode(array('success' => true));
				} else{
				echo json_encode(array('success' => false, 'msg' => 'Sorry. User Not Found.'));
			}
		}
	}
	
	//forgot password link
	public function pwreset($forgot_pwd_token =null){
		$this->layout = 'admin';
		$this->User->recursive= -1;
		if($this->request->is('post')){
			if(!empty($forgot_pwd_token) && strlen($this->request->data['User']['password']) >= 6
			&& $this->request->data['User']['password'] == $this->request->data['User']['confirm_password']){
				$checkuser = $this->User->findByForgotPwdToken($forgot_pwd_token, array('User.id'));
				if(!empty($checkuser)){
					$data['User']['forgot_pwd_token'] = null;
					$data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
					$this->User->id = $checkuser['User']['id'];
					if($this->User->save($data)){
						$this->Session->setFlash(__('Your password is successfully reset. Please go back to your app and login with your new password.'));
						$this->redirect(array('action' => 'pwreset', 'success'));
					}
					} else {
					$this->Session->setFlash(__('Invalid password reset token.'), 'default', array('class' => 'error-message message'));
					$this->redirect(array('action' => 'pwreset'));
				}
				} else {
				$this->Session->setFlash(__('Password must be at least 6 character and make sure it matches with confirm password.'), 'default', array('class' => 'error-message message'));
				$this->redirect($this->referer());
			}
		}
		$this->set(compact('forgot_pwd_token'));
	}
	

	//device token added
	private function _device_token($user_id, $device_token, $type, $stage, $old_device_token= null){
	    $this->User->DeviceToken->recursive = -1;
	    if(empty($old_device_token))
	    	$check_device_token = $this->User->DeviceToken->findByDeviceToken($device_token,array('DeviceToken.id','DeviceToken.device_token'));
	   	else 
	   		$check_device_token = $this->User->DeviceToken->findByDeviceToken($old_device_token,array('DeviceToken.id','DeviceToken.device_token'));
	    if($check_device_token){
	        $check_device_token['DeviceToken']['user_id'] = $user_id;
	        $check_device_token['DeviceToken']['stage'] = empty($stage) ? 'production' : $stage;
	        $check_device_token['DeviceToken']['device_token'] = $device_token;
	        $this->User->DeviceToken->id = $check_device_token['DeviceToken']['id'];
	        $this->User->DeviceToken->save($check_device_token);

	    }else{
	        $this->User->DeviceToken->create();
	        $check_device_token = array();
	        $check_device_token['DeviceToken']['user_id'] = $user_id;
			$check_device_token['DeviceToken']['device_type'] = $type;
	        $check_device_token['DeviceToken']['device_token'] = $device_token;
	        $check_device_token['DeviceToken']['stage'] = empty($stage) ? 'production' : $stage;
	        $this->User->DeviceToken->save($check_device_token);      
	    }
  	}

  	public function update_device_token(){
  		$this->autoRender = false;
  		$user_id = $this->request->data['User']['id'];
  		$device_token = $this->request->data['User']['device_token'];
  		$type = $this->request->data['User']['type'];
  		$stage = $this->request->data['User']['stage'];
  		$old_device_token = $this->request->data['User']['old_device_token'];
  		$this->_device_token($user_id, $device_token, $type, $stage, $old_device_token);
  	}

	//upload image
	private function _upload($file){
		App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));
		if(is_uploaded_file($file['tmp_name'])){
			$ext  = strtolower(array_pop(explode('.',$file['name'])));
			if($ext == 'txt') $ext = 'jpg';
			$fileName = time() . rand(1,999) . '.' .$ext;
			if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext=='gif'){
				$uplodFile = WWW_ROOT.'files'.DS.'driver_infos'. DS .$fileName;
				if(move_uploaded_file($file['tmp_name'],$uplodFile)){
					$dest_small = WWW_ROOT . 'files' . DS . 'driver_infos' . DS . 'small' . DS . $fileName;
					if($this->_resize($uplodFile, $dest_small)){
						return $fileName;
					}
					//echo(json_encode(array('filename' => $fileName) ));die;	
					else die(json_encode(array('success' => false, 'msg' => "Image couldn't resize.") ));
				}
			}
		}
	}

	//image resize
	private function _resize($src, $dest_small){
		$phpThumb = new phpThumb();
		$phpThumb->resetObject();
		$capture_raw_data = false;
		$phpThumb->setSourceFilename($src);
		$phpThumb->setParameter('w', 200);
		//$phpThumb->setParameter('w', 1184);
		//$phpThumb->setParameter('h', 852);
		$phpThumb->setParameter('h', 150);
		//$phpThumb->setParameter('zc', 1);
		$phpThumb->GenerateThumbnail();
		$phpThumb->RenderToFile($dest_small);
		return true;
	}
	
	/**
	* Update User's Position
	*/
/*	
	public function utrac() {
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data) && !empty($this->request->data['User']['id'])) {
			// Prevent hack for any field update
			$data['User'] = array(
			'id' => $this->request->data['User']['id'],
			'lat' => $this->request->data['User']['lat'],
			'lng' => $this->request->data['User']['lng'],
			);
			if($this->User->save($data, false)) {
				// get drivers count if this request from a driver
				$user_type = $this->User->findById($this->request->data['User']['id'], array('type'));
				if($user_type['User']['type'] == 'driver'){
					$driver_count = $this->User->get_available_drivers_around_driver($this->request->data['User']['id'], $this->request->data['User']['lat'], $this->request->data['User']['lng']);
					die(json_encode(array('success' => true, 'no_of_drivers' => $driver_count)));
					} else {
					die(json_encode(array('success' => true)));
				}
			} else die(json_encode(array('success' => false, 'message' => 'Lat-Lng Update failed.')));
		} else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}
	*/
	
	//searching driver to bar
	public function search_driver(){

	}

	/**
	*	Search Drive for Bar Action
	*/
	public function sdvr() {
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)) {
			$this->User->recursive = -1;
			
			// Check for Query parameters
			if(!empty($this->request->data['User']['name']) && empty($this->request->data['User']['badge_no'])) unset($this->request->data['User']['badge_no']);
			else if(empty($this->request->data['User']['name']) && !empty($this->request->data['User']['badge_no'])) unset($this->request->data['User']['name']);
			else if (empty($this->request->data['User']['name']) && empty($this->request->data['User']['badge_no'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
			else if(empty($this->request->data['User']['code'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
			
			$passenger = $this->User->findByCode($this->request->data['User']['code'], array('id'));
			// Get previous barred drivers by this passenger
			//$already_barred = $this->User->Barred->find('all', array('fields' => array('Barred.userId2'), 'conditions' => array('Barred.userId1' => $passenger['User']['id'], 'Barred.who_to_whom' => 'p2d')));

			if(empty($this->request->data['User']['name']) && !empty($this->request->data['User']['badge_no'])){
				$con = array(
					'User.type' => 'driver',
					'User.badge_no LIKE' => '%' . $this->request->data['User']['badge_no'] . '%'
				);
			} else if(empty($this->request->data['User']['badge_no']) && !empty($this->request->data['User']['name'])){
				$con = array(
					'User.type' => 'driver',
					'User.name LIKE ' => '%'.$this->request->data['User']['name'].'%'
				);
			} else if(!empty($this->request->data['User']['badge_no']) && !empty($this->request->data['User']['name'])){
				$con = array(
					'User.type' => 'driver',
					'OR' => array(
						'User.name LIKE ' => '%'.$this->request->data['User']['name'].'%',
						'User.badge_no LIKE ' => '%'.$this->request->data['User']['badge_no'].'%'

					)
				);
			}
		
			// Find Drivers
			$users = $this->User->find('all', array(
				'fields' => array('User.id','User.name', 'User.badge_no', 'User.email', 'User.image'), 
				'conditions' => array(
						'User.type' => 'driver',
						'User.name LIKE' => '%'.$this->request->data['User']['name'].'%',
						'User.badge_no LIKE' => '%' . $this->request->data['User']['badge_no'] . '%'		
					), 
				'order' => array('User.name' => 'desc')));

			if(empty($users)) {
				die(json_encode(array('success' => false, 'message' => 'No records found')));
				} else {
				$drivers = array();
				foreach($users as $user) {
					$drivers[]['Driver'] = $user['User'];
				}
				die(json_encode(array('success' => true, 'drivers' => $drivers)));
			}
		} else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}
	
	/**
	*	Search Passengers for Bar action
	*/
	public function spsngrs() {
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)) {
			/*$this->request->data['User']['name'] = 'tam';
			$this->request->data['User']['mobile'] = '234234';
			$this->request->data['User']['code'] = '528c5d2d-7f8c-48aa-a67b-08a41603f7ec'; */
			$this->User->recursive = -1;
			//print_r($this->request->data);
			
			// Check for Query parameters
			if(!empty($this->request->data['User']['name']) && empty($this->request->data['User']['mobile'])) unset($this->request->data['User']['mobile']);
			else if(empty($this->request->data['User']['name']) && !empty($this->request->data['User']['mobile'])) unset($this->request->data['User']['name']);
			else if(empty($this->request->data['User']['name']) && empty($this->request->data['User']['mobile'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
			else if(empty($this->request->data['User']['code'])) die(json_encode(array('success' => false, 'message' => 'Invalid Search')));
			
			
			$driver_id = $this->User->findByCode($this->request->data['User']['code'], array('id'));

			// Get previous barred passengers by this driver
			//$already_barred = $this->User->Barred->find('all', array('fields' => array('Barred.userId2'), 'conditions' => array('Barred.userId1' => $driver_id['User']['id'], 'Barred.who_to_whom' => 'd2p')));
			if(!empty($this->request->data['User']['mobile']) && empty($this->request->data['User']['name'])){
				$con = array(
					'User.type' => 'passenger',
					'User.mobile LIKE' => '%' . $this->request->data['User']['mobile'] . '%'
				);
			}
			else if(empty($this->request->data['User']['mobile']) && !empty($this->request->data['User']['name'])){
				$con = array(
					'User.type' => 'passenger',
					'User.name LIKE' => '%'.$this->request->data['User']['name'].'%'
				);
			} else if (!empty($this->request->data['User']['mobile']) && !empty($this->request->data['User']['name'])){
				$con = array(
					'User.type' => 'passenger',
					'OR' => array(
						'User.name LIKE' => '%'.$this->request->data['User']['name'].'%',
						'User.mobile LIKE' => '%' . $this->request->data['User']['mobile'] . '%'
					)
				);
			}
			
			// Find passengers
			$users = $this->User->find('all', array(
					'fields' => array('User.id', 'User.name', 'User.mobile', 'User.email'), 
					'conditions' => $con,
			'order' => array('User.name' => 'desc')));

			if(empty($users)) {
				die(json_encode(array('success' => false, 'message' => 'No records found')));
			} else {
				$passengers = array();
				foreach($users as $user) {
					$passengers[]['Passenger'] = $user['User'];
				}
				die(json_encode(array('success' => true, 'passengers' => $passengers)));
			}
		} else die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
	}
	
	/**
	*	Get list of all Bar drivers/passengers
	*/
	public function gbard($code=null, $type='driver') {
		$this->autoRender = false;
		if(empty($code)) die(json_encode(array('success' => false, 'message' => 'Invalid Request')));
		
		$this->User->recursive = -1;
		$user = $this->User->findByCode($code, array('id', 'type'));
		if(empty($user)) die(json_encode(array('success' => false, 'message' => 'Invalid User')));
		
		// Switch user type
		$flag = ($type == 'driver') ? 'p2d' : 'd2p';
		$barred = $this->User->Barred->find('all', array('fields' => array('id', 'userId2', 'who_to_whom'), 'conditions' => array('Barred.who_to_whom' => $flag,'Barred.userId1' => $user['User']['id'])));
		if(empty($barred)) die(json_encode(array('success' => false, 'message' => 'No record found')));
		else {
			$barred_ids = Hash::extract($barred, '{n}.Barred.userId2');
			if ($type == 'driver') {
				$barred_users = $this->User->find('all', array('fields' => array('id', 'name', 'email', 'mobile', 'badge_no', 'code'), 'conditions' => array('User.id' => $barred_ids)));
			} else {
				$barred_users = $this->User->find('all', array('fields' => array('id', 'name', 'email', 'mobile', 'code'), 'conditions' => array('User.id' => $barred_ids)));
			}

			foreach($barred_users as $index => $user) {
				$barred[$index]['Barred']['User'] = $user['User'];
			}
			die(json_encode(array('success' => true, 'allBars' => $barred)));
		}
	}
	
	
	/**
	*	Check the position of an existing driver in queues and a new driver in the users table
	*/
	private function _position_phone($user_id = null, $availablilty_type='phonecall_available', $zone_id=null) {
		
		$zone_field = str_replace('available','zone',$availablilty_type);
		$booking_request_count = $this->User->query("SELECT COUNT(id) AS total_request FROM phone_queues WHERE user_id='$user_id'");
		if($booking_request_count[0][0]['total_request']>0) {
			
			$position = $this->User->query('
			SELECT
			COUNT(tt.id)+ 1 AS position
			FROM
			(
			SELECT
			t.*
			FROM
			(
			SELECT
			q.*
			FROM
			phone_queues q
			INNER JOIN users u ON (q.user_id=u.id)
			WHERE u.' . $availablilty_type . ' = \'yes\' AND u.is_enabled = 1 AND u.' . $zone_field .' = \'' . $zone_id . '\'
			ORDER BY
			request_time DESC
			)AS t
			GROUP BY
			t.user_id
			ORDER BY
			t.request_time
			)AS tt
			WHERE
			tt.request_time <(
			SELECT
			MAX(request_time)
			FROM
			phone_queues
			WHERE
			user_id =  \'' . $user_id . ' \'
			)'
			);
			return $position[0][0]['position'];
			
			
			} else { // this will never run
			
			$total_in_queue = $this->User->query('SELECT
			COUNT(DISTINCT q.user_id)AS total
			FROM
			queues q
			INNER JOIN users u ON(
			q.user_id = u.id
			AND u.is_enabled = 1
			AND u.' . $availablilty_type . '= \'yes\'
			AND u.' . $zone_field .' = \'' . $zone_id . '\'
			)'
			);
		}
		return null;
		
	}
	
	private function _position($user_id = null, $availablilty_type='vr_available', $zone_id=null) {
		
		//$zone_field = str_replace('available','zone',$availablilty_type);
		$booking_request_count = $this->User->query("SELECT COUNT(id) AS total_request FROM queues WHERE user_id='$user_id'");
		//print_r($user_id); 
		if($booking_request_count[0][0]['total_request']>0) {
			
			$position = $this->User->query("SELECT
			COUNT(tt.id)+ 1 AS position
			FROM
			(
			SELECT
			t.*
			FROM
			(
			SELECT
			q.*
			FROM
			queues q
			INNER JOIN users u ON (q.user_id=u.id)
			WHERE u.vr_available  = 'yes' AND u.is_enabled = '1' AND u.vr_zone = '$zone_id'
			ORDER BY
			request_time DESC
			)AS t
			GROUP BY
			t.user_id
			ORDER BY
			t.request_time
			)AS tt
			WHERE
			tt.request_time <(
			SELECT
			MAX(request_time)
			FROM
			queues
			WHERE
			user_id =  '$user_id'
			)"
			);
			return $position[0][0]['position'];
			
			
			} else { // this will never run

				//print_r('what the hell???');
			
			$total_in_queue = $this->User->query('SELECT
			COUNT(DISTINCT q.user_id)AS total
			FROM
			queues q
			INNER JOIN users u ON(
			q.user_id = u.id
			AND u.is_enabled = 1
			AND u.' . $availablilty_type . '= \'yes\'
			AND u.' . $zone_field .' = \'' . $zone_id . '\'
			)'
			);
		}
		return null;
		
	}

	
	
	private function _zone_info($type_zone, $lat=null, $lng=null, $user_id) {
		
		if(empty($lat) && empty($lng))
		{
			return null;
		}
		$conditions = array(
			"CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))",
			'Zone.type'=> $type_zone
			);
		if ($type_zone == 'vr_zone'){
			$join = array(
				'table' => 'users_zones',
				'alias'=> 'UsersZone',
				'type' => 'LEFT',
				'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id') 
				);
			$conditions['UsersZone.id'] = null;
		} else { //phonecall_gps_zone
			$join = array(
				'table' => 'users_zones',
				'alias'=> 'UsersZone',
				'type' => 'INNER',
				'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id')
				);
		}
		$this->loadModel('Zone');
		$zones = $this->Zone->find('all',array(
			'recursive' => -1,
			'joins' => array($join),
			'conditions' => $conditions,
			'fields' => array(
			'Zone.id', 'Zone.polygon'
			)
			)
		);
		foreach ($zones as $key => $zone) {
			$points_string = str_replace(array('POLYGON', '(', ')', ','), array('','', '', ','), $zone['Zone']['polygon']);
			$points = explode(',', $points_string);
			$result =  $this->_inside_zone($points, $lat, $lng);
			if($result){
				$id = $zone['Zone']['id'];
				break;
			}
		}
		if ($result){
			return $id;
		} else {
			return null;
		}
		
	}
	
	/**
	*	Check if driver available and return driver's position
	*/
	public function isdav($code=null, $availability_type=null, $set_driver_availability=null, $return = false) {
		//Configure::write('debug', 2);
		$this->autoRender = false;
		if(empty($code)) die(json_encode(array('success' => true, 'message' => 'Invalid Request')));
		$now = date("Y-m-d H:i:s");
		$time_pre = microtime(true);
		$this->User->recursive = -1;
		$user = $this->User->findByCode($code);
		$user_id = $user['User']['id'];
		
		$this->User->id = $user_id; // for update

		$driver_zone = $user['User']['vr_zone'];
		$driver_zone_phone = $user['User']['phonecall_zone'];
		$driver_zone_gps = $user['User']['phonecall_gps_zone'];
		if($user['User']['type'] == 'driver') {

			//check if the driver / passenger is enabled or not
			if($user['User']['is_enabled'] == 0) {
					$response = array(
					'success' => false, 
					'message' => 'You are not enabled yet. Please wait. You will be notified after activation. Thank You.'
				);
			} else {

				if($this->request->is('post') && !empty($this->request->data)){
					// /print_r('expression'); exit;
					$lat = $this->request->data['User']['lat'];
					$lng = $this->request->data['User']['lng'];
					
					$user_data = array();
					if($this->request->data['User']['is_vr'] == 1)
					{
						//print_r('is_vr'); exit;
						$zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
						$user_data=array('User'=>array(
							//'vr_available'=>empty($zone_id) ? 'no' : 'yes',
							'vr_available'=>'yes',
							'vr_zone' => $zone_id,
							'lat' => $lat,
							'lng' => $lng,
							'last_activity' => $now
						));
						$this->User->save($user_data);
//						$this->_position_driver($zone_id);
//						$update_queue = $this->User->query('UPDATE queues SET response = "ignore" WHERE id = "$user['User']['id']" AND response = "timeout"');
					}
					if($this->request->data['User']['is_phone_gps'] == 1)
					{
						$zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
						$user_data=array('User'=>array(
							//'phonecall_gps_available'=>empty($zone_id) ? 'no' : 'yes',
							'phonecall_gps_available' => 'yes',
							'phonecall_gps_zone' => $zone_id,
							'lat' => $lat,
							'lng' => $lng,
							'last_activity' => $now
						));
						$this->User->save($user_data);
					}
					if($this->request->data['User']['is_phone'] == 1)
					{
						if(!empty($user['User']['phonecall_zone']))
							$user_data=array('User'=>array('phonecall_available'=>'yes',
								'lat' => $lat,
								'lng' => $lng,
								'last_activity' => $now));
						else {
							$msg = 'You are not assigned to any phone-call zone.';
							$user_data=array('User'=>array('phonecall_available'=>'no',
								'lat' => $lat,
								'lng' => $lng));
						}
						$this->User->save($user_data);
					}
					$user_data['User']['lat'] = $lat;
					$user_data['User']['lng'] = $lng;
					$user_data['User']['last_activity'] = $now;
					$this->User->save($user_data);
					
				}

				if(!empty($set_driver_availability) && in_array(trim($set_driver_availability), array('yes', 'no'))) {
					//print_r('step 2'); exit;
					$lat = $lng = null;
					if($this->request->is('post') && !empty($this->request->data)) {
						$lat = $this->request->data['User']['lat'];
						$lng = $this->request->data['User']['lng'];
					}
					$user_data = array();
					switch ($availability_type) {
						case 'vr':
							$zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
							$user_data=array('User'=>array(
							//'vr_available'=>empty($zone_id) ? 'no' : trim($set_driver_availability),
							'vr_available'=> trim($set_driver_availability),
							'vr_zone' => $zone_id
							));
						break;
						case 'phone_gps':
							$zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
							$user_data=array('User'=>array(
							//'phonecall_gps_available'=>empty($zone_id) ? 'no' : trim($set_driver_availability),
							'phonecall_gps_available'=> trim($set_driver_availability),
							'phonecall_gps_zone' => $zone_id
						));
						break;
						case 'phone':
							if(!empty($user['User']['phonecall_zone']))
								$user_data=array('User'=>array('phonecall_available'=>trim($set_driver_availability)));
							else {
								$msg = 'You are not assigned to any phone-call zone.';
								$user_data=array('User'=>array('phonecall_available'=>'no'));
							} 
						break;
						case 'all':
						default:
							$vr_zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
							$phonecall_gps_zone_id = $this->_zone_info('phonecall_gps_zone', $lat, $lng, $user_id);
							$user_data=array('User'=>array(
								//'vr_available'=>empty($vr_zone_id) ? 'no' : trim($set_driver_availability),
								'vr_available'=> trim($set_driver_availability),
								'vr_zone' => $vr_zone_id,
								//'phonecall_gps_available'=>empty($phonecall_gps_zone_id) ? 'no' : trim($set_driver_availability),
								'phonecall_gps_available'=> trim($set_driver_availability),
								'phonecall_gps_zone' => $phonecall_gps_zone_id,
								'phonecall_available'=>empty($user['User']['phonecall_zone']) ? 'no' : trim($set_driver_availability)
							));
						break;
					}
					if(!empty($lat) && !empty($lng)){
						$user_data['User']['lat'] = $lat;
						$user_data['User']['lng'] = $lng;
						$user_data['User']['last_activity'] = $now;
					}
					
					//$this->User->id = $user_id;
					$this->User->save($user_data);
				}
				
				//update user's position and send push to positioned changed drivers
				$this->_position_driver($driver_zone);
				$this->_position_driver_phone($driver_zone_phone);
				$this->_position_driver_gps($driver_zone_gps);
				
				$user = $this->User->find('first', array(
					'fields' => array('*'),
					'joins' => array(array(
						'table' => 'zones',
						'alias' => 'VRZone',
						'type' => 'LEFT',
						'conditions' => array('User.vr_zone = VRZone.id')
					),array(
						'table' => 'zones',
						'alias' => 'PhoneGPSZone',
						'type' => 'LEFT',
						'conditions' => array('User.phonecall_gps_zone = PhoneGPSZone.id')
					),array(
						'table' => 'zones',
						'alias' => 'PhoneZone',
						'type' => 'LEFT',
						'conditions' => array('User.phonecall_zone = PhoneZone.id')
					)),
					'conditions' => array('User.code' => $code)
				));
				$vr_available = $user['User']['vr_available'] == 'yes' ? true : false;
				$vr_position = $vr_available ? $this->_position($user_id, 'vr_available', $user['User']['vr_zone']) : null ;
				
				$phonecall_gps_available = $user['User']['phonecall_gps_available'] == 'yes' ? true : false;
				$phonecall_gps_position = $phonecall_gps_available ? $this->_position_phone($user_id, 'phonecall_gps_available', $user['User']['phonecall_gps_zone']) : null ;
				
				$phonecall_available = $user['User']['phonecall_available'] == 'yes' ? true : false;
				$phonecall_position = $phonecall_available ? $this->_position_phone($user_id, 'phonecall_available', $user['User']['phonecall_zone']) : null ;
				
				if($user['User']['vr_available'] == 'yes'){
					$update_queue = $this->User->query("UPDATE queues SET response = 'ignored' WHERE user_id = '$user_id' AND response = 'timeout'");
				//	$this->_position_driver($user['User']['vr_zone']);
				}
				if($user['User']['phonecall_available'] == 'yes'){
					$update_queue = $this->User->query("UPDATE phone_queues SET status = 'ignored' WHERE user_id = '$user_id' AND status IS NULL AND completed IS NULL AND DATE_ADD(created, INTERVAL 1 MINUTE)< '$now'");
				//	$this->_position_driver($user['User']['vr_zone']);
				}
				if($user['User']['phonecall_gps_available'] == 'yes'){
					$update_queue = $this->User->query("UPDATE phone_queues SET status = 'ignored' WHERE user_id = '$user_id' AND status IS NULL AND completed IS NULL AND DATE_ADD(created, INTERVAL 1 MINUTE)< '$now'");
				//	$this->_position_driver($user['User']['vr_zone']);
				}

				if($driver_zone != $user['User']['vr_zone'] && $vr_available == 'yes') { //checks if the driver has moved into another zone
					//update user's position and send push to positioned changed drivers
					$this->_position_driver($user['User']['vr_zone']);
				}

				if($driver_zone != $user['User']['phonecall_gps_zone'] && $phonecall_gps_available == 'yes') { //checks if the driver has moved into another zone
					//update user's position and send push to positioned changed drivers
					$this->_position_driver_gps($user['User']['phonecall_gps_zone']);
				}

//print_r(var_export($this->request->data, true)); exit;

				$response = array(
					'success' => true,
					'vr' => $vr_available,
					'vr_position' => $vr_position,
					'vr_zone_id' => $user['VRZone']['id'],
					'vr_zone' => $user['VRZone']['name'],
					'vr_btn' => (empty($user['VRZone']['name']) ? 'Nearest Driver' : $user['VRZone']['name']) . ((empty($user['VRZone']['name']) || empty($vr_position)) ? '' : ' - P' . $vr_position),
					'phone_gps' => $phonecall_gps_available,
					'phone_gps_position' => $phonecall_gps_position,
					'phone_gps_zone_id' => $user['PhoneGPSZone']['id'],
					'phone_gps_zone' => $user['PhoneGPSZone']['name'],
					'phone_gps_btn' => (empty($user['PhoneGPSZone']['name']) ? 'N/A' : $user['PhoneGPSZone']['name']) . ((empty($user['PhoneGPSZone']['name']) || empty($phonecall_gps_position)) ? '' : ' - P' . $phonecall_gps_position),
					'phone' => $phonecall_available,
					'phone_position' => $phonecall_position,
					'phone_zone_id' => $user['PhoneZone']['id'],
					'phone_zone' => $user['PhoneZone']['name'],
					'phone_btn' => (empty($user['PhoneZone']['name']) ? 'N/A' : $user['PhoneZone']['name']) . (empty($phonecall_position) ? '' : ' - P' . $phonecall_position),
					'msg'=>''
					);
			}
			// API log	
			$this->_apiLog(
			$user_id, 
			var_export($this->request->data, true), 
			var_export($response, true), 
			1, 
			$time_pre);

			if($return)
				return $response;
			else 
				die(json_encode($response));
		} else die(json_encode(array('success' => false, 'message' => 'You are not a driver.')));
	}
	
	// private function _push($device_token, $message, $app){
	// 	App::import('Vendor', 'UA', array('file' => 'UA' . DS . 'push.php'));
	// 	$sendPush = new SendPush();
	// 	$sendPush->push($device_token, $message, $app);
	// }
	
	/**
	*	Emergency button
	*/
	public function emergency($is_over=0){
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)){
			$driver = array();
			$driver_id = $this->request->data['User']['id'];
			$driver_info = $this->User->findById($driver_id);
			$lat = $this->request->data['User']['lat'];
			$lng = $this->request->data['User']['lng'];
			$address = (strlen($this->request->data['User']['address']) > 50) ? substr($this->request->data['User']['address'],0,50).'...' : $this->request->data['User']['address'];
			$driver['name'] = $driver_info['User']['name'];
			$driver['address'] = $address;
			$driver['lat'] = $lat;
			$driver['lng'] = $lng;
			$driver['mobile'] = $driver_info['User']['mobile'];

			$available_drivers = $this->User->get_available_drivers_around_driver($driver_id, $lat, $lng, 1, false);
			$is_over = trim($is_over);
			if(!empty($available_drivers)) {
				// $device_tokens = Hash::extract($available_drivers, '{n}.User.id');
				// foreach($device_tokens as $device_token) {
				// 	$devices[] = $device_token;
				// }
				$driver_ids = Hash::extract($available_drivers, '{n}.User.id');
				foreach($driver_ids as $id) {					
				 	if($is_over == 1 || $is_over == 'cancel') {
						$this->_push($id, 'I am '. $driver_info['User']['name'] .'('. $driver_info['User']['mobile'] .') was in emergency at ' . $address . '.Right now emergency is over.', 'CabbieCall', null, null, null, null, null, null, null, null, null, null, true, $driver_id);
					} else {
						$this->_push($id, 'I am '. $driver_info['User']['name'] .'in danger, at: ' . $address . '. Please help me.','CabbieCall', null, null, null, null, null, null, null, null, null, null, true, $driver_id);
					}
				}
				
				die(json_encode(array('success' => true, 'danger' => $driver)));
			} else die(json_encode(array('success' => false, 'message' => __('No Driver to send notification'))));
		} else die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
	}

	//locate driver in emergency by other drivers
	public function driver_in_danger_info($id, $info = null){
		$this->autoRender = false;
			$driver_id = $id;
			$driver_info = $this->User->findById($driver_id);
			$driver['name'] = $driver_info['User']['name'];
			$driver['lat'] = $driver_info['User']['lat'];
			$driver['lng'] = $driver_info['User']['lng'];
			$driver['mobile'] = $driver_info['User']['mobile'];
			if($info == 'info') die(json_encode(array('success' => true, 'danger' => $driver)));
			if($info == 'coordinate') die(json_encode(array('success' => true, 'lat' => $driver['lat'], 'lng' => $driver['lng'])));
	}
	
	
	/**
	*	Get User's active balance
	*/
	public function guab($code) {
		$this->autoRender = false;
		if(empty($code)) die(json_encode(array('success' => false, 'message' => __('Invalid request'))));
		$user = $this->User->findByCode($code, array('id', 'type'));
		if(empty($user['User']['id'])) die(json_encode(array('success' => false, 'message' => __('Invalid User'))));
		//$active_balance = $this->User->getUserActiveBalance($user['User']['id']);
		$this->loadModel('Transaction');
		if($user['User']['type'] == 'driver') {
			$balance = $this->Transaction->getDriverBalance($user['User']['id']);
			//pr($balance);
		} else {
			$balance = $this->Transaction->getPassengerBalance($user['User']['id']);
		}
		//print_r($balance);
		//$earning = empty($balance[2][0]['total']) ? 0 : $balance[2][0]['total'];
		$this->loadModel('Setting');
		$min = $this->Setting->findById(1);
		$min_transferable_balance = $min['Setting']['balance_trans'];
		$driver_msg = $min['Setting']['driver_msg'];
		$free_credit = $balance[1][0]['total'];
		$total_fee = abs($balance[2][0]['total']);
		$total = $balance[4][0]['total'];
		$balance_value = $this->Transaction->calFreeCredit($free_credit, $total_fee, $total);
		$remaining_free_credit = $balance_value['remaining_free_credit'];
		$total = empty($balance_value['total']) ? 0 : $balance_value['total'];
		if($user['User']['type'] == 'driver') {
			echo json_encode(array('success' => true, 'balance' => $total, 'min_transferable_balance' => $min_transferable_balance, 'driver_msg' => $driver_msg, 'free_credit' => $remaining_free_credit));
		} else {
			echo json_encode(array('success' => true, 'balance' => $balance[0][0]['total']));
		}
	}
	
	/**
	*	Verify user's number via Twilio
	*/
	
	private function _twilio_verify($number, $name) {
		App::import('Vendor', 'Twilio', array('file' => 'Twilio' . DS . 'Twilio.php'));
		$sid = 'ACced6e5e9b0ba4d6dabf280cf2bd05b64';
		$token = '7c36c41e156b77ef4c40db7bca2ed432';
		$client = new Services_Twilio($sid, $token);
		$caller_id = $client->account->outgoing_caller_ids->create($number, array(
		"FriendlyName" => $name . "\'s Number"
		));
		return $caller_id->sid;
	}
	
	public function vun() {
		$this->autoRender = false;
		if($this->request->is('post') && !empty($this->request->data)) {
			$number = $this->request->data['User']['number'];
			$name = $this->request->data['User']['name'];
			$sid = $this->_twilio_verify($number, $name);
			die(json_encode(array('success' => true, 'sid' => $sid)));
		} else die(json_encode(array('success' => false, 'message' => __('Invalid Request.'))));
	}
	
	// view of user's top up record

	public function admin_topup($user_id = null, $limit_or_from = 30, $to = null) {
		// /Configure::write('debug', 2);
		if (!$this->User->exists($user_id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->User->useTable = 'transactions';
	    //$this->User->recursive = -1;
		
		if(!empty($limit_or_from) && !empty($to)){
			$from = $limit_or_from;
			$conditions = array('Transaction.is_paid' => 1, 'User.id' => $user_id, 'DATE(Transaction.created) BETWEEN ? AND ? ' => array($from, $to));
		} else {
			$limit_created = $limit_or_from;
			$conditions = array('Transaction.is_paid' => 1, 'User.id' => $user_id, 'TIMESTAMPDIFF(DAY,Transaction.created,NOW()) <=' => $limit_created);
		}

		$this->paginate = array(
	        'limit' => 25,
	        'joins' => array(
	            array(
		            'table' => 'transactions',
		            'alias' => 'Transaction',
		            'type' => 'left',
		            'foreignKey' => false,
		            'conditions' => array('User.id = Transaction.user_id')
		            ),array(
		            'table' => 'users',
		            'alias' => 'ReferredUser',
		            'type' => 'left',
		            'foreignKey' => false,
		            'conditions' => array('Transaction.referred_user_id = ReferredUser.id')
		            )),
	        'conditions' => $conditions,
	        'fields' => array('Transaction.*', 'User.name', 'User.id', 
	        	'User.type', 'User.account_holder_name', 
	        	'User.account_no', 'User.swift','User.iban','User.sort_code','ReferredUser.name','ReferredUser.id'),
	       	'order' => array('Transaction.created' => 'desc'),
	    );
		$user = $this->User->find('first', array(
			'recursive' => -1,
			'conditions' => array('User.id' => $user_id),
			'fields' => array('type','admin_percentage', 'name'),
		));
		if($user['User']['type'] == 'driver'){
			$sum_amount = $this->User->Transaction->getDriverBalance($user_id);		
		} else {
			$sum_amount = $this->User->Transaction->getPassengerBalance($user_id);
		}
		$free_credit = $sum_amount[1][0]['total'];
		$total_fee = abs($sum_amount[2][0]['total']);
		$actual_total = $sum_amount[0][0]['total'];
		$actual_balance = $this->User->Transaction->calFreeCredit($free_credit, $total_fee, $actual_total);
		$available_total = $sum_amount[4][0]['total'];
		$available_balance = $this->User->Transaction->calFreeCredit($free_credit, $total_fee, $available_total);
		$transactions = $this->paginate();
		$this->set(compact('transactions'));
		$this->set(compact('user_id', 'limit_created', 'from', 'to','sum_amount', 'actual_balance', 'available_balance', 'user'));
	}

	//give free credit to any user

	public function admin_freetoup($id){
		
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->loadModel('Transaction');
			$this->request->data['Transaction']['user_id'] = $id;
			$now = date('Y-m-d');
			$this->request->data['Transaction']['timestamp'] = $now;
			$this->Transaction->create();
			if($this->Transaction->save($this->request->data, false)) {
				$this->Session->setFlash(__('Free Credit has been given successfully.'));
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('Free Credit could not be saved. Please, try again.'));
			}
		}
		$this->User->recursive = -1;
		$user = $this->User->find('all', array(
			'conditions' => array('User.id' => $id),
			'fields' =>  array('User.name','User.type')
			));
		$this->set(compact('user'));
	}

	//enable new driver
	public function admin_enable_driver($id = null){
		$this->autoRender = false;
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		$user = $this->User->findById($id, array('email', 'name', 'type'));
		$this->request->data['User']['is_enabled'] = 1;
		$this->request->data['User']['first_update'] = 0;
		if ($this->User->save($this->request->data, false)) {
			//print_r($user['User']['email']);
			//exit;
			try{
				$this->_sendEmail(
				$user['User']['email'], 
				'Cabbie App',
				"Dear ".$user['User']['name'].",<br/><br/>Your account has been activated.<br/><br/>Thanks.<br/><br/>Admin",
				'default'
				); 
			} catch (Exception $e){

			}
			
			if($user['User']['type'] == 'driver'){
				$this->_push($id, 'Your account has been activated.', 'CabbieCall');
			} else {
				$this->_push($id, 'Your account has been activated.', 'CabbieApp');				
			}
			$this->Session->setFlash(__('The user has been enabled.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('The user could not be enabled. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	//disable any user
	public function admin_disable_driver($id = null){
		$this->autoRender = false;
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		$user = $this->User->findById($id, array('email', 'name', 'type'));
		$this->request->data['User']['is_enabled'] = 0;		
		$this->request->data['User']['phonecall_available'] = 'no';	
		$this->request->data['User']['phonecall_gps_available'] = 'no';	
		$this->request->data['User']['vr_available'] = 'no';	
		$this->request->data['User']['vr_zone'] = null;
		$this->request->data['User']['phonecall_gps_zone'] = null;
		$this->request->data['User']['phonecall_zone'] = null;

		if ($this->User->save($this->request->data, false)) {
			//print_r($user['User']['email']);
			//exit;
			try{
				$this->_sendEmail(
				$user['User']['email'], 
				'Cabbie App',
				"Dear ".$user['User']['name'].",<br></br>Your account has been deactivated.<br/><br/>Thanks.<br/><br/>Admin",
				'default'
				); 
			} catch (Exception $e){

			}
			
			//print_r($user['User']['email']);
			if($user['User']['type'] == 'driver'){
				$this->_push($id, 'Your account has been deactivated. Please contact Admin.','CabbieCall');
			} else {
				$this->_push($id, 'Your account has been deactivated. Please contact Admin.','CabbieApp');
			}
			$this->Session->setFlash(__('The user has been disabled.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('The user could not be disabled. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

		// send email to the admin when new driver sign up
	private function _activateEmail($id = null, $is_update = null){
		$this->loadModel('Setting');
		$receiver = $this->Setting->findById(1, array('admin_email'));	
		$user = $this->User->findById($id);	
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('smtp');
		$email->subject('Cabbie App — Activate new Driver');
		$email->to($receiver['Setting']['admin_email']);
		$path = Router::url(array('controller' => 'users', 'action' => 'index', 'admin' => true, 'driver'), true ); 
		if($is_update){
			$email->send("Admin, <br/> Please activate the driver named ". $user['User']['name'] . ".<br/> The driver has updated 'Edit My Details'. <br/> To activate, please click on " .$path. "<br/> Thanks");
		} else {
			$email->send("Admin, <br/> Please activate the driver named ". $user['User']['name'] . ".<br/> To activate, please click on " .$path. "<br/> Thanks");
		}
		
	}

	public function req_fr_promoter(){
		$this->autoRender = false;
		$promoter = $this->User->find('first', array(
				'conditions' => array('User.type' => 'admin'),
				'fields' => array('User.promoter_code')
			));
		die(json_encode(array('success' => true, 'promoter' => $promoter['User']['promoter_code'])));
	}

	//public function test($type_zone, $lat=null, $lng=null, $user_id) {
	public function test($id = null) {		

		//echo Router::url(array('controller' => 'users', 'action' => 'index', 'admin' => true, driver), true ); 
		$this->autoRender = false;
		$this->_push($id, 'driver push.','CabbieCall');
		//$this->_push($id, 'testing push by Belicia.','CabbieApp');
		// $payee_id = '144';
		// $service == 'new_paypal';
		// if($service != 'credit_transfer') {
  //                       if($service == 'new_paypal') $service = 'paypal';
		// 				$this->_push($payee_id, 'Payment received successfully via '.$service. '.','CabbieCall');
		// 			}

		// //print_r($type_zone);
		// if(empty($lat) && empty($lng))
		// {
		// 	return null;
		// }
		// $conditions = array(
		// 	"CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))",
		// 	'Zone.type'=> $type_zone
		// 	);
		// if ($type_zone == 'vr_zone'){
		// 	$join = array(
		// 		'table' => 'users_zones',
		// 		'alias'=> 'UsersZone',
		// 		'type' => 'LEFT',
		// 		'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id') 
		// 		);
		// 	$conditions['UsersZone.id'] = null;
		// } else { //phonecall_gps_zone
		// 	$join = array(
		// 		'table' => 'users_zones',
		// 		'alias'=> 'UsersZone',
		// 		'type' => 'INNER',
		// 		'conditions' => array('UsersZone.user_id = \''. $user_id . '\' AND Zone.id = UsersZone.zone_id')
		// 		);
		// }
		// $this->loadModel('Zone');
		// $zones = $this->Zone->find('all',array(
		// 	'recursive' => -1,
		// 	'joins' => array($join),
		// 	'conditions' => $conditions,
		// 	'fields' => array(
		// 	'Zone.id', 'Zone.polygon'
		// 	)
		// 	)
		// );
		// //print_r($zones);
		// foreach ($zones as $key => $zone) {
		// 	$points_string = str_replace(array('POLYGON', '(', ')', ','), array('','', '', ','), $zone['Zone']['polygon']);
		// 	$points = explode(',', $points_string);
		// 	$result =  $this->_inside_zone($points, $lat, $lng);
		// 	if($result){			
		// 		$id = $zone['Zone']['id'];
		// 		break;
		// 	}
		// }
		// if ($result){
		// 	return $id;
		// } else {
		// 	return null;
		// }
		
	}

	private function _inside_zone($polygon_points, $lat, $lng){
		App::import('Vendor', 'polygon', array('file' => 'polygon' . DS . 'point_in_polygon.php'));
		$point = "$lat $lng";
		$polygon = $polygon_points;
		// print_r($point);
		// print_r($polygon);
		$pointLocation = new pointLocation();
		//print "Point in " . $pointLocation->pointInPolygon($point, $polygon) . " the polygon";
		if($pointLocation->pointInPolygon($point, $polygon) === 'inside') {
			return true;
		} else return null;

	}

	
	private function _save_user($my_promoter_code = null){
		$this->request->data['User']['code'] = String::uuid();
		$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

        /*File Upload*/
        if(!empty($this->request->data['User']['imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['imagex']['tmp_name'])){
            $this->request->data['User']['image'] = $this->_upload($this->request->data['User']['imagex']);
        }

        if(!empty($this->request->data['User']['badge_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['badge_imagex']['tmp_name'])){
            $this->request->data['User']['badge_image'] = $this->_upload($this->request->data['User']['badge_imagex']);
            print_r($this->request->data);die;
        }
        if(!empty($this->request->data['User']['insurance_certificate_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_certificate_imagex']['tmp_name'])){
            $this->request->data['User']['insurance_certificate_image'] = $this->_upload($this->request->data['User']['insurance_certificate_imagex']);

        }
        if(!empty($this->request->data['User']['vehicle_licence_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['vehicle_licence_imagex']['tmp_name'])){
            $this->request->data['User']['vehicle_licence_image'] = $this->_upload($this->request->data['User']['vehicle_licence_imagex']);
        }
        /*In v2 added this */
        if(!empty($this->request->data['User']['drivers_licence_imagex_front']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_front']['tmp_name'])){
            $this->request->data['User']['drivers_licence_image_front'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_front']);
        }
        if(!empty($this->request->data['User']['drivers_licence_imagex_back']['tmp_name']) && is_uploaded_file($this->request->data['User']['drivers_licence_imagex_back']['tmp_name'])){
            $this->request->data['User']['drivers_licence_image_back'] = $this->_upload($this->request->data['User']['drivers_licence_imagex_back']);
        }

       /* if(!empty($this->request->data['User']['insurance_imagex']['tmp_name']) && is_uploaded_file($this->request->data['User']['insurance_imagex']['tmp_name'])){
            $this->request->data['User']['insurance_image'] = $this->_upload($this->request->data['User']['insurance_imagex']);
        }*/
        /*End File Upload*/
        $userInfo = $this->request->data['User'];

		$this->User->create();
		if($this->User->save($this->request->data, false)){
							
			$id = $this->User->id;
			if(!empty($this->request->data['User']['device_token'])){
				if(empty($this->request->data['User']['device_type'])){
					$this->request->data['User']['device_type'] = 'android';
				}
				$this->_device_token($id, $this->request->data['User']['device_token'],
				$this->request->data['User']['device_type'], $this->request->data['Application']['stage']);
			}
			// free credit for new sign up 
			$this->loadModel('Setting');
			$data_setting = $this->Setting->findById(1);	
			if($this->request->data['User']['type'] == 'driver') 
			{	
				$this->_new_driver_in_queue($id); // add new driver in queue
				$balance = $data_setting['Setting']['free_credit_driver'];
				try{
					//send email to admin for activation
					$this->_activateEmail($id);
				}catch(Exception $e){

                }
			}
			else {
				$balance = $data_setting['Setting']['free_credit_passenger'];
			}
			$this->_add_free_credit($id, $balance); 
			
			if($this->request->data['User']['type'] == 'driver'){
				die(json_encode(array(
					'success' => true, 
					'User' => array(
						'id' => $this->User->id, 
						'code' => $this->request->data['User']['code'], 
						'promoter_code' => $my_promoter_code,

                        'name' => $userInfo['name'],
                        'ref_promoter_code' => $userInfo['ref_promoter_code'],
                        'email' => $userInfo['email'],
                        'mobile' => $userInfo['mobile'],
                        'address' => $userInfo['address'],
                        'home_no' => $userInfo['home_no'],
                        'post_code' => $userInfo['post_code'],
                        'cab_type' => $userInfo['cab_type'],
                        'vehicle_type' => $userInfo['vehicle_type'],
                        'is_wheelchair' => $userInfo['is_wheelchair'],
                        'vehicle_licence_no' => $userInfo['vehicle_licence_no'],
                        'vehicle_licence_image' => $userInfo['vehicle_licence_image'],
                        'image' => $userInfo['image'],
                        'badge_no' => $userInfo['badge_no'],
                        'badge_image' => $userInfo['badge_image'],
                        'registration_plate_no' => $userInfo['registration_plate_no'],
                        'insurance_certificate' => $userInfo['insurance_certificate'],
                        'insurance_certificate_image' => $userInfo['insurance_certificate_image'],
                        'type' => $userInfo['type'],
                        
                        'no_of_seat' => $userInfo['no_of_seat'],
                        'voip_no' => $userInfo['voip_no'],

                        'branch_number' => $userInfo['branch_number'],
                        'driving_licence_no' => $userInfo['driving_licence_no'],
                        'drivers_licence_image_front' => $userInfo['drivers_licence_image_front'],
                        'drivers_licence_image_back' => $userInfo['drivers_licence_image_back'],
                        'driving_licence_dateExpiry' => $userInfo['driving_licence_dateExpiry'],

                        'running_distance' => $userInfo['running_distance']
					))));
			} else {
				die(json_encode(array(
					'success' => true, 
					'User' => array(
						'id' => $this->User->id, 
						'code' => $this->request->data['User']['code']
					))));
			}
		} else die(json_encode(array('success' => false, 'message' => 'Registration failed.')));
	}

	public function is_updated_info($code){
		$this->autoRender = false;
		$user_obj = $this->User->find('first', array(
			'recursive' => -1,
			'conditions' => array('User.code' => $code, 
				'User.first_update' => 0,
				'User.is_enabled' => 0
				),
			'fields' => array('User.id')
		));
		//print_r($user_obj);
		if(empty($user_obj)) die(json_encode(array('success' => true)));
		else die(json_encode(array('success' => false))); // pop up appears on App
	}

	public function balance_check($user_id){
		$this->autoRender = false;
		$user_obj = $this->User->findById($user_id);
		if(!empty($user_obj) && $user_obj['User']['type'] == 'driver'){
			$this->loadModel('Setting');
        	$driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
        	$min_balance = $driver_min_balance['Setting']['driver_min_balance'];
			$balance = $this->User->query("SELECT
					*
				FROM
					users AS q
				WHERE
					(
						SELECT
							SUM(amount)
						FROM
							transactions
						WHERE
							user_id = q.id
						AND is_paid = 1
						AND service <> 'cash_in_hand'
					)>= '$min_balance' AND q.id = '$user_id'
			");

			if(empty($balance)) die(json_encode(array('success' => false, 'message' => 'Low balance, Please top-up.')));
			else die(json_encode(array('success' => true)));
		} else die(json_encode(array('success' => false, 'message' => 'Invalid user')));
	}

    /*********====================New Functions=========================*********/
    public function vendor_registration(){
        $this->autoRender = false;
//        echo '<pre>';
//        print_r($this->request->data['User']['picture']);die;
//        echo '</pre>';
        $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
        if($this->request->data['User']['type'] == 'vendor'){
            $this->request->data['User']['name'] = $this->request->data['User']['company_name'];
            $this->request->data['User']['vendor_license_image'] = $this->_upload($this->request->data['User']['license_picture'], 'vendor_infos');
        }
        $this->request->data['User']['created'] = date('Y-m-d H:i:s');
        $this->User->create();

        if($this->User->save($this->request->data['User'])){
            $this->Session->setFlash(__('Registration Successful.'), 'default', array('class' => 'alert alert-success'));
        }else{
            $this->Session->setFlash(__('Registration Not Successful'), 'default', array('class' => 'alert alert-danger'));
        }
        $this->redirect($this->referer());
        $this->admin_login($this->request->data);
    }

    public function near_by_drivers($lat, $lng, $user_id)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $vr_zone_id = $this->_zone_info('vr_zone', $lat, $lng, $user_id);
        //echo $vr_zone_id;die;
        $this->loadModel('UsersZone');
        $query = array(
            //'recursive' => 1,
            /* 'joins' => array(
                 array(
                     'table' => 'users',
                     'alias'=> 'User',
                     'type' => 'LEFT',
                     'conditions' => array('User.id = UsersZone.user_id')
                 )
             ),
             'fields' => array(
                 'User.*'
             ),*/
            'conditions' => array(
                'UsersZone.zone_id' => $vr_zone_id,
                'not' => [
                    'UsersZone.user_id' => $user_id,
                ],
            )
        );
        $data = $this->UsersZone->find('all', $query);
        if (!empty($data)) {
            $result = Hash::extract($data, '{n}.UsersZone');
            die(json_encode(array('success' => true, 'drivers' => $result)));
        } else die(json_encode(array('success' => false, 'msg' => 'No Result found.')));
    }

    public function validate_input()
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        // pr(key($this->params->query['data']));die;
        $key = key($this->params->query['data']['User']);
        $username = $this->params->query['data']['User'][$key];

        /*if($key=='email') {
            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                return $this->response->statusCode(400);
            }
        }*/

        $this->User->recursive = -1;
        $is_exist = $this->User->find('first', [
            'conditions' => ['User.' . $key => $username]
        ]);
        if (!empty($is_exist)) {
            return $this->response->statusCode(400);
        } else {
            return $this->response->statusCode(200);
        }
    }

    public function admin_despatch_job(){
        $auth = $this->Auth->user();
        $ss = $this->Session->read('Auth');
//        echo '<pre>';
//        print_r($ss);
//        echo '</pre>';
        $user_id = $ss['User']['id'];
        if($ss['User']['type'] == 'admin'){   // this will be vendor instead of admin
            $branch_number = $ss['User']['branch_number'];
            $this->User->recursive = -1;
            $driver_list = $this->User->find('all', ['conditions'=>['User.type'=>'driver','User.branch_number'=>$branch_number]]);
        }

//        $pageContents = $this->Staticpage->findBySlug($slug);
//        $page = $pageContents['Staticpage'];

        $this->loadModel('Staticpage');
        $footerContents = $this->Staticpage->findByMenuTitle('footer');
        $footer = $footerContents['Staticpage'];
        $links = $this->Staticpage->find('all', array(
                'fields' => array('Staticpage.slug', 'Staticpage.menu_title', 'Staticpage.sort_order'),
                'conditions' => array('Staticpage.slug <> ' => 'footer'),
                'order' => array('Staticpage.sort_order' => 'asc')
            )
        );

        $this->loadModel('Setting');
        $header_links = $this->Setting->find('all', array(
                'fields' => array('Setting.itunes_link', 'Setting.googleplay_link', 'Setting.facebook_link',
                    'Setting.twitter_link', 'Setting.website_contact_email')
            )
        );
        $this->set(compact('auth', 'page', 'driver_list', 'subpage', 'title_for_layout', 'footer', 'links', 'page', 'header_links', 'dynamic_info', 'home_content'));
        $view = '/Pages/despatch_job';
        $this->render($view);
    }
    
}

