<?php
App::uses('AppController', 'Controller');
/**
* Zones Controller
*
* @property Zone $Zone
*/
class ZonesController extends AppController {
	
	
		public $components = array('Paginator');
	/**
	* admin_index method
	*
	* @return void
	*/

/*Buy Twilio Number*/
	public function admin_available_phone_numbers(){
// /		Configure::write('debug', 2);
		App::import('Vendor', 'Services', array('file' => 'Services' . DS . 'Twilio.php'));	
	    /* Set CabbieApp's AccountSid and AuthToken */
	    $AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
	    $AuthToken = "c67580685ab040d98a11612441b87848";
	 
	    /* Instantiate a new Twilio Rest Client */
	    $client = new Services_Twilio($AccountSid, $AuthToken);
        try {
	            /* Initiate Uk Local PhoneNumber search with $SearchParams list */
	            $numbers = $client->account->available_phone_numbers->getList('GB', 'Local', array("VoiceEnabled" => "true"));
        } catch (Exception $e) {}
        // Loop over the list of numbers and echo a property for each one
	        $existing_purchage_twilio_numbers = array();
	        foreach ($client->account->incoming_phone_numbers as $number) {
   				$existing_purchage_twilio_numbers[] = array('number' => $number->phone_number, 'sid' => $number->sid);
	    		}
	    		//print_r($existing_purchage_twilio_numbers); exit;
	    	unset($existing_purchage_twilio_numbers[4]);
        $this->set(compact('numbers','existing_purchage_twilio_numbers'));
	}
	public function admin_buy_twilio_phone_numbers($PhoneNumber = null){
//		Configure::write('debug', 2);
		if(!empty($PhoneNumber)){
			App::import('Vendor', 'Services', array('file' => 'Services' . DS . 'Twilio.php'));	
		    /* Set CabbieApp's AccountSid and AuthToken */
	    	$AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
	   		$AuthToken = "c67580685ab040d98a11612441b87848";
		 
		    /* Instantiate a new Twilio Rest Client */
		    $client = new Services_Twilio($AccountSid, $AuthToken);
	        try {
		      	 /* Initiate Uk Local PhoneNumber search with $SearchParams list */
		            $number = $client->account->incoming_phone_numbers->create(array(
	                'PhoneNumber' => $PhoneNumber,
	                'VoiceUrl' => 'http://cabbieappuk.com/phone_queues/twilio_call',
	            ));
	        } catch (Exception $e) {}
	        $this->Session->setFlash(__("Thank you for purchasing $PhoneNumber"));
			$this->redirect(array('action'=> 'available_phone_numbers' ));
	    }
	}
	public function admin_delete_twilio_phone_numbers($PhoneNumber = null){
//		Configure::write('debug', 2);
		if(!empty($PhoneNumber)){
			App::import('Vendor', 'Services', array('file' => 'Services' . DS . 'Twilio.php'));	
		    /* Set CabbieApp's AccountSid and AuthToken */
	    	$AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
	    	$AuthToken = "c67580685ab040d98a11612441b87848";
		 
		    /* Instantiate a new Twilio Rest Client */
		    $client = new Services_Twilio($AccountSid, $AuthToken);
	        try {
		        $client->account->incoming_phone_numbers->delete($PhoneNumber);
	        } catch (Exception $e) {}
	        $this->Session->setFlash(__("You have deleted Successfully."));
			$this->redirect(array('action'=> 'available_phone_numbers' ));
	    }
	}
	public function admin_index($zone_type = 'vr_zone') {
		$this->Zone->recursive = -1;
		$zones = $this->Zone->find('all', array(
			'conditions' => array(
				'Zone.type' => $zone_type,
				'Zone.id NOT IN(1051, 1072, 1086, 1087, 1089, 1088)'
				)));
		$polygons = array();
		foreach($zones as $i => $zone){
			if(!empty($zone['Zone']['polygon_points'])){
				$polygons[$i]['poly'] = json_decode($zone['Zone']['polygon_points']);
				$polygons[$i]['name'] = $zone['Zone']['name'];
				$polygons[$i]['id'] = $zone['Zone']['id'];
			}
		}
		$this->set(compact('polygons', 'zone_type', 'zones'));
	}
	
	/**
	* admin_view method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_view($id = null) {
		if (!$this->Zone->exists($id)) {
			throw new NotFoundException(__('Invalid zone'));
		}
		$options = array('conditions' => array('Zone.' . $this->Zone->primaryKey => $id));
		$this->set('zone', $this->Zone->find('first', $options));
	}

	public function proxy() {
		$this->autoRender = false;
		App::uses('HttpSocket', 'Network/Http');
		$pickup_postcode = $this->params->query['term'];
		$HttpSocket = new HttpSocket();
		$postcode = $HttpSocket->get('http://appinstitute.co.uk/services/getAddress.php?',
			array(
				 'postcode' => $pickup_postcode
				)
		);		
		echo($postcode->body);
	}
	
	/**
	* admin_add method
	*
	* @return void
	*/
	
	public function admin_add($zone_type = 'vr_zone') {
		//debug($this->reqeust->data);	
		if ($this->request->is('post')) {
			if(((!empty($this->request->data['Zone']['polygon_points']) && $this->request->data['Zone']['type'] != 'phonecall_zone') &&
				(!empty($this->request->data['Zone']['polygon']) && $this->request->data['Zone']['type'] != 'phonecall_zone')
				|| $this->request->data['Zone']['type'] == 'phonecall_zone' ))  {
					$voip_check= $this->Zone->find('first', array(
						'conditions' => array('voip_phone_no' => $this->request->data['Zone']['voip_phone_no'],  'Zone.type <>'=> 'vr_zone')));
					if(!empty($voip_check['Zone']['voip_phone_no'])){
						$this->Session->setFlash(__('Duplicate VOIP No.'));
						return $this->redirect(array('action' => 'index', $zone_type));
					} else {
						$this->Zone->create();
						if($this->request->data['Zone']['type'] == 'phonecall_zone') {
							$this->request->data['Zone']['polygon'] = '';
							$this->request->data['Zone']['polygon_points'] = '';
						}
						if ($this->Zone->save($this->request->data)) {
						$this->Session->setFlash(__('The zone has been saved'));
						$this->redirect(array('action' => 'index', $this->request->data['Zone']['type']));
						} else {
						$this->Session->setFlash(__('The zone could not be saved. Sorry! Something went wrong.'));
						$this->redirect(array($this->request->data['Zone']['type']));
						}	
					}
					
			} else {
				$this->Session->setFlash(__('The zone could not be saved. Please, try again.'));
				$this->redirect(array($this->request->data['Zone']['type']));
			}	
		} else {
			$this->Zone->recursive = -1;
			$zones = $this->Zone->find('all', array(
			 	'conditions' => array('Zone.type' => $zone_type, 'Zone.id NOT IN(1051, 1072, 1086, 1087, 1089, 1088)')
			 	//'conditions' => array('Zone.type' => $zone_type)
			));
			$polygons = array();
			foreach($zones as $i => $zone){
				if(!empty($zone['Zone']['polygon_points'])){
					$polygons[$i]['poly'] = json_decode($zone['Zone']['polygon_points']);
					$polygons[$i]['name'] = $zone['Zone']['name'];
					$polygons[$i]['id'] = $zone['Zone']['id'];
				}
			}
			if($zone_type != 'vr_zone'){
				$existing_purchase_twilio_numbers = $this->_purchased_twilio_numbers();
			//	print_r($existing_purchase_twilio_numbers); exit;
				unset($existing_purchase_twilio_numbers['+441484598251']);
				
			}
			$this->set(compact('polygons', 'zone_type', 'zones','existing_purchase_twilio_numbers'));
		
		}
		
	}
	private function _purchased_twilio_numbers(){
		App::import('Vendor', 'Services', array('file' => 'Services' . DS . 'Twilio.php'));	
			    /* Set CabbieApp's AccountSid and AuthToken */
	    		$AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
	    		$AuthToken = "c67580685ab040d98a11612441b87848";
			 
			    /* Instantiate a new Twilio Rest Client */
			    $client = new Services_Twilio($AccountSid, $AuthToken);
			    $existing_purchase_twilio_numbers = array();
	        	foreach ($client->account->incoming_phone_numbers as $number) {
	    				$existing_purchase_twilio_numbers[$number->phone_number] = $number->phone_number;
	    		}
	    		return $existing_purchase_twilio_numbers;
	}
	
	/**
	* admin_edit method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_edit($id = null) {
		if (!$this->Zone->exists($id)) {
			throw new NotFoundException(__('Invalid zone'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$check = $this->Zone->find('first', array('conditions' => array('Zone.voip_phone_no' => $this->request->data['Zone']['voip_phone_no'], 'Zone.id <>' => $this->request->data['Zone']['id'], 'Zone.type <>'=> 'vr_zone')));
 		 	if (!$check) {
				if ($this->Zone->save($this->request->data)) {
					$this->Session->setFlash(__('The zone has been saved'));
					$this->redirect(array('action' => 'index', $this->request->data['Zone']['type']));
				} else {
					$this->Session->setFlash(__('The zone could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The zone could not be saved as duplicate data have found.'));
				$this->redirect(array('action' => 'index', $this->request->data['Zone']['type']));
			}
		} else {

			$options = array('conditions' => array('Zone.' . $this->Zone->primaryKey => $id));
			$this->request->data = $this->Zone->find('first', $options);
			if($this->request->data['Zone']['type'] != 'vr_zone'){
				$existing_purchase_twilio_numbers = $this->_purchased_twilio_numbers();
				//print_r($existing_purchase_twilio_numbers); exit;
				unset($existing_purchase_twilio_numbers['+441484598251']);
				$this->set(compact('existing_purchase_twilio_numbers'));
			}
		}
	}
	
	/**
	* admin_delete method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function admin_delete($id = null) {
		$this->Zone->id = $id;
		if (!$this->Zone->exists()) {
			throw new NotFoundException(__('Invalid zone'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Zone->delete()) {
			$this->loadModel('PhoneSetting');
			$this->PhoneSetting->query('DELETE FROM phone_settings WHERE zone_id = \'' . $id . ' \'
			');
			$this->loadModel('VrSetting');
			$this->VrSetting->query('DELETE FROM vr_settings WHERE zone_id = \'' . $id . ' \'
			');
			$this->loadModel('FareSetting');
			$this->FareSetting->query('DELETE FROM fare_settings WHERE zone_id = \'' . $id . ' \'
			');
			$this->Session->setFlash(__('Zone deleted'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Zone was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	//polygon data update
	public function update($polygon){
		$this->autoRender = false;
		$this->Zone->save($this->request->data);
	}

	//function to display Virtual Rank info 
	public function g($zone_type = 'vr'){ 
		$this->autoRender =  false;
		$zones = $this->Zone->find('all', array(
			'fields' => array('COUNT(DISTINCT User.id) AS taxis','COUNT(DISTINCT Booking.id) AS jobs','Zone.name'),
			'recursive' => -1,
			'group' => array('Zone.id'),
			'conditions' => array('Zone.type' => $zone_type.'_zone'),
			'joins' => array(
	            array(
		            'table' => 'users',
		            'alias' => 'User',
		            'type' => 'LEFT',
		            'foreignKey' => false,
		            'conditions' => array( 
		            	'User.'.$zone_type.'_zone = Zone.id',
		            	'User.'.$zone_type.'_available' => 'yes')
		            ),
	            array(
		            'table' => 'bookings',
		            'alias' => 'Booking',
		            'type' => 'LEFT',
		            'foreignKey' => false,
		            'conditions' => array( 
		            	//'Booking.zone_id = Zone.id',
		            	//'Booking.acceptor' => null,
		            	"(Booking.status NOT IN ('cancelled_by_passenger') AND Booking.acceptor IS NULL AND (Booking.is_sap_booking <> '1' OR Booking.is_sap_booking IS NULL) AND Booking.zone_id = Zone.id) 
		            	OR 
		            	(Booking.status NOT IN ('cancelled_by_passenger', 'no_driver_found') AND Booking.is_sap_booking = '1' AND Booking.acceptor IS NULL) AND Booking.zone_id = Zone.id")
		            )
			)));

		echo json_encode(array('success' => true, 'zones' => $zones));
	}

	//logout offline user 
	public function cronjobZoneSettings(){
		$this->autoRender = false;
		$now = date('Y-m-d H:i:s');

		$this->_remove_inactive_device_token();
		
		$this->_logout_inactive_phone();

		$this->_logout_inactive_gps();

		$this->email_send_cronjob();

		$push_users = $this->Zone->query("SELECT
					u.id,
					u.last_activity,
					u.vr_zone,
					IF(z.allow_inactive_time IS NULL, 90, z.allow_inactive_time),
					TIMESTAMPDIFF(
							MINUTE,
							u.last_activity,
							'$now'
						)
					FROM
						users u
					LEFT JOIN zones z ON(
						z.id = u.vr_zone
					)
					WHERE
					u.type = 'driver' AND
					u.vr_available = 'yes' AND
					(
						TIMESTAMPDIFF(
							MINUTE,
							u.last_activity,
							'$now'
						)
					)>
					IF(
						z.allow_inactive_time IS NULL,
						90,
						z.allow_inactive_time
					)");
//		print_r($push_users);
//		print_r($now);
		$unique_zones = array();
		foreach ($push_users as $push_user) {
			$driver_zone = $push_user['u']['vr_zone'];
			$driver_id = $push_user['u']['id'];
			if(!in_array($driver_zone, $unique_zones)) {
				$unique_zones[] = $driver_zone;
			}
			// send notification to driver
	       	$this->_push($driver_id, 'You have been logged-out from VR Zone.','CabbieCall', null, null, null, true);
			$logout_user = $this->Zone->query("UPDATE users u SET u.vr_available = 'no', u.vr_zone = NULL WHERE u.vr_available = 'yes' AND 
					u.type = 'driver' AND u.id = '$driver_id'"
				); 
		}
		foreach ($unique_zones as $unique_zone) {
			//send push msg to drivers for position change after log-out
			$this->_position_driver($unique_zone);
		}

		// inactive drivers from VR zone, default time 90 min
	/*	$getusrs = $this->Zone->query("
			UPDATE users u LEFT JOIN zones z ON(z.id = u.vr_zone AND u.vr_available = 'yes')
				SET u.vr_available = 'no', u.vr_zone = NULL	WHERE z.type = 'vr_zone' AND 
					u.type = 'driver' AND (TIMESTAMPDIFF(MINUTE, u.last_activity, '$now')) >
				IF(z.allow_inactive_time IS NULL, 90, z.allow_inactive_time)
						");  */

	}

	private function _logout_inactive_phone(){
		$now = date('Y-m-d H:i:s');
		$push_users = $this->Zone->query("SELECT
						u.id,
						u.last_activity,
						u.phonecall_zone,
						z.allow_inactive_time,
					 TIMESTAMPDIFF(MINUTE, u.last_activity, '$now')
					FROM
						users u
					INNER JOIN phone_settings z ON(z.zone_id = u.phonecall_zone)
					WHERE
						u.type = 'driver'
					AND u.phonecall_available = 'yes'
					AND(TIMESTAMPDIFF(MINUTE, u.last_activity, '$now')
					)> z.allow_inactive_time");
//		print_r($push_users);
//		print_r($now);
		$unique_zones = array();
		foreach ($push_users as $push_user) {
			$driver_zone = $push_user['u']['vr_zone'];
			$driver_id = $push_user['u']['id'];
			if(!in_array($driver_zone, $unique_zones)) {
				$unique_zones[] = $driver_zone;
			}
			// send notification to driver
	       	$this->_push($driver_id, 'You have been logged-out from Phone Call Zone.','CabbieCall', null, null, null, null, null, null, null, null, null, null, null, null, true);
			$logout_user = $this->Zone->query("UPDATE users SET phonecall_available  = 'no'
					WHERE id = '$driver_id' AND phonecall_available = 'yes'"
				); 
		}

		foreach ($unique_zones as $unique_zone) {
			//send push msg to drivers for position change after log-out
			$this->_position_driver_phone($unique_zone);
		}
	}

	private function _logout_inactive_gps(){
		$now = date('Y-m-d H:i:s');
		$push_users = $this->Zone->query("SELECT
						u.id,
						u.last_activity,
						u.phonecall_gps_zone,
						TIMESTAMPDIFF(
							MINUTE,
							u.last_activity,
							'$now'
						)
					FROM
						users u
					INNER JOIN phone_settings z ON(
						z.zone_id = u.phonecall_gps_zone
					)
					WHERE
						u.type = 'driver'
					AND u.phonecall_gps_available = 'yes'
					AND(
						TIMESTAMPDIFF(
							MINUTE,
							u.last_activity,
							'$now'
						)
					)> z.allow_inactive_time");
//		print_r($push_users);
//		print_r($now);
		$unique_zones = array();
		foreach ($push_users as $push_user) {
			$driver_zone = $push_user['u']['vr_zone'];
			$driver_id = $push_user['u']['id'];
			if(!in_array($driver_zone, $unique_zones)) {
				$unique_zones[] = $driver_zone;
			}
			// send notification to driver
	       	$this->_push($driver_id, 'You have been logged-out from Phone GPS Zone.','CabbieCall', null, null, null, null, null, null, null, null, null, null, null, null, null, true);
			$logout_user = $this->Zone->query("UPDATE users SET phonecall_gps_available= 'no', phonecall_gps_zone =NULL 
					WHERE id = '$driver_id' AND phonecall_gps_available = 'yes'"
				); 
		}
		foreach ($unique_zones as $unique_zone) {
			//send push msg to drivers for position change after log-out
			$this->_position_driver_gps($unique_zone);
		}
	}

	private function _remove_inactive_device_token(){
		$this->loadModel('DeviceToken');
		$delete_device_tokens = $this->DeviceToken->query('
            DELETE FROM device_tokens WHERE DATE(modified) < DATE_SUB(CURDATE(),INTERVAL 1 MONTH)');
	}

	//to auto set response = cancelled in the queue tbl so that drivers do not get notification
	public function email_send_cronjob(){
		$this->autoRender = false;
		$now_check = date("Y-m-d H:i:s");
		$get_bookings = $this->Zone->query("SELECT * FROM	bookings WHERE TIMESTAMPDIFF(MINUTE, at_time, NOW())>= 1 AND status = 'no_driver_found' AND is_sap_booking <> '1' AND notify_admin = '0'");
		if(!empty($get_bookings)){
			$this->loadModel('Setting');
			$receiver = $this->Setting->findById(1, array('admin_email'));
			App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail('smtp');
				$Email->viewVars(array('get_bookings' =>  $get_bookings));
				$Email->emailFormat('html');
				$Email->template('autocancel');
				$Email->to($receiver['Setting']['admin_email']);
				$Email->subject('Driver Not Found!!!');
				$Email->send();
				//echo 'email send';
			
			$booking_ids = array();
			foreach ($get_bookings as $get_booking) {
				$booking_ids[] = $get_booking['bookings']['id'];
			}
			$this->Zone->query("UPDATE bookings SET notify_admin = '1' WHERE id IN (".implode(',', $booking_ids).")");	
			
		}
		$this->Zone->query("
			UPDATE queues SET response = 'auto_cancelled' WHERE 
			TIMESTAMPDIFF(HOUR, created, '$now_check') >=1 AND (response = 'timeout' OR response = 'ignored')
		");

		//$this->test();
		//$this->_push(600, 'corn job zones running','CabbieCall');

	}
	public function test(){
		$this->autoRender = false;
		App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail('smtp');
				//$Email->viewVars(array('get_bookings' =>  $get_bookings));
				$Email->emailFormat('html');
				$Email->template('autocancel');
				$Email->to('rob@appinstitute.co.uk');
				$Email->subject('this email is send by cronjobZoneSettings -cabbieapp');
				$Email->send();
	}

}