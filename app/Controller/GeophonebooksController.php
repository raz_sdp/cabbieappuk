<?php
App::uses('AppController', 'Controller');
/**
 * Geophonebooks Controller
 *
 * @property Geophonebook $Geophonebook
 * @property PaginatorComponent $Paginator
 * @property nComponent $n
 */
class GeophonebooksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Geophonebook->recursive = 0;
		$this->set('geophonebooks', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Geophonebook->exists($id)) {
			throw new NotFoundException(__('Invalid geophonebook'));
		}
		$options = array('conditions' => array('Geophonebook.' . $this->Geophonebook->primaryKey => $id));
		$this->set('geophonebook', $this->Geophonebook->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Geophonebook->create();
			if ($this->Geophonebook->save($this->request->data)) {
				$this->Session->setFlash(__('The geophonebook has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The geophonebook could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Geophonebook->exists($id)) {
			throw new NotFoundException(__('Invalid geophonebook'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Geophonebook->save($this->request->data)) {
				$this->Session->setFlash(__('The geophonebook has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The geophonebook could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Geophonebook.' . $this->Geophonebook->primaryKey => $id));
			$this->request->data = $this->Geophonebook->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Geophonebook->id = $id;
		if (!$this->Geophonebook->exists()) {
			throw new NotFoundException(__('Invalid geophonebook'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Geophonebook->delete()) {
			$this->Session->setFlash(__('The geophonebook has been deleted.'));
		} else {
			$this->Session->setFlash(__('The geophonebook could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * nearestPhoneNo method
 *
 * @return nearest taxi cab phone number
 */
	public function nearestPhoneNo($lat,$lng){
	
		$this->autoRender = false;
		$phone = $this->Geophonebook->find('first',array(
			'fields' => array('Geophonebook.phone','ROUND(1000*6371.0*ACOS(SIN('. $lat .'*PI()/180)*SIN(Geophonebook.lat*PI()/180)+COS('. $lat .'*PI()/180)*
								COS(Geophonebook.lat*PI()/180)*COS(('. $lng .'*PI()/180)-(Geophonebook.lng*PI()/180)))) AS distance'
								),
			'order' => array('distance')
			));
		
		echo json_encode($phone['Geophonebook']); 

	}


}
