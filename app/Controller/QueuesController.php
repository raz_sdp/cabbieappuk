<?php
App::uses('AppController', 'Controller');
/**
 * Queues Controller
 *
 * @property Queue $Queue
 */
class QueuesController extends AppController {

/**
 * cleanup method
 * Cron Job to remove null entries from queues table which were added during driver signup to 
 * enqueue a driver in queue loop based on signup time to assign him a rank.
 * @return void
 */

	// not running now as diplaying data for booking not for driver
	/* public function cleanup() {
		$this->autoRender =  false;
		$this->Queue->recursive = -1;
		
		// select those entries from queues table which have booking_id null and 
		// that corresponding user_id has at least another with booking_id not null
		
		$this->Queue->query('DELETE q
			FROM queues q
			INNER JOIN (
				SELECT id
				FROM queues
				WHERE booking_id IS NULL
					AND user_id IN (
						SELECT user_id
						FROM queues
						WHERE booking_id IS NOT NULL
						GROUP BY user_id
						HAVING count(user_id) > 0
						)
				) qx ON qx.id = q.id');
	} */

	//public function cleanup(){
	// 	$this->autoRender = false;
	// 	$log_path = APP.'tmp' . DS . 'logs'. DS .'error.log';
	// 	$fp = fopen($log_path, "w");
	// 	fwrite($fp, " ");
	// 	fclose($fp);
	// 	$debug_log_path = APP.'tmp' . DS . 'logs'. DS .'debug.log';
	// 	$filep = fopen($debug_log_path, "w");
	// 	fwrite($filep, " ");
	// 	fclose($filep);
	//}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($booking_id = null) {
		$this->Queue->recursive = 0;
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('Queue.booking_id' => $booking_id),
	        'order' => 'Queue.created DESC'
	    );
	   	$queues = $this->paginate();
		$this->set(compact('queues','booking_id'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Queue->exists($id)) {
			throw new NotFoundException(__('Invalid queue'));
		}
		$options = array('conditions' => array('Queue.' . $this->Queue->primaryKey => $id));
		$this->set('queue', $this->Queue->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Queue->create();
			if ($this->Queue->save($this->request->data)) {
				$this->Session->setFlash(__('The queue has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The queue could not be saved. Please, try again.'));
			}
		}
		$zones = $this->Queue->Zone->find('list');
		//CakeLog::write('debug', var_export($zones, true));
		$users = $this->Queue->User->find('list');
		$this->set(compact('users', 'zones'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Queue->exists($id)) {
			throw new NotFoundException(__('Invalid queue'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Queue->save($this->request->data)) {
				$this->Session->setFlash(__('The queue has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The queue could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Queue.' . $this->Queue->primaryKey => $id));
			$this->request->data = $this->Queue->find('first', $options);
		}
		$zones = $this->Queue->Zone->find('list');
		$users = $this->Queue->User->find('list');
		$this->set(compact('users', 'zones'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Queue->id = $id;
		if (!$this->Queue->exists()) {
			throw new NotFoundException(__('Invalid queue'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Queue->delete()) {
			$this->Session->setFlash(__('Queue deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Queue was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	

	public function clear_log(){
		$this->autoRender = false;
		$log_path = APP.'tmp' . DS . 'logs'. DS .'error.log';
		$fp = fopen($log_path, "w");
		fwrite($fp, " ");
		fclose($fp);
		$debug_log_path = APP.'tmp' . DS . 'logs'. DS .'debug.log';
		$filep = fopen($debug_log_path, "w");
		fwrite($filep, " ");
		fclose($filep);
	}

    //queue refresh list
	public function booking($start_time = null){
		//set_time_limit(0);
		
		/* Newrelic. Ignore wp-cron.php transactions. Are REAL slow */
			// if (extension_loaded('newrelic')) {
			// 	newrelic_ignore_transaction (TRUE);
			// 	newrelic_ignore_apdex (TRUE);
			// }‏
		/////////////////////////////////////////////////////////////

		ini_set('max_execution_time', 0);
		if(empty($start_time)){
			$GLOBALS['cronStart'] = microtime(true);
		}

		

		$this->autoRender = false;	
		
		/**
		* 1. timeout bookings
		* 2. Assign jobs to next drivers
		*/
		
		//booking auto timeout  after 3 mins
		$now = date('Y-m-d H:i:s');
		$this->_timeout_bookings();
		//print_r($timeout_list);
		// get bookings
		$bookings = $this->Queue->Booking->find('all', 
			array(
				'recursive' => 0,
				'joins' => array(
					array(
						'table' => 'queues',
						'alias' => 'Queue',
						'type' => 'LEFT',
						'conditions' => 'Queue.booking_id = Booking.id'
						),
					array(
						'table' => 'zones',
						'alias' => 'Zone',
						'type' => 'LEFT',
						'conditions' => '(Zone.id = Booking.zone_id 
								AND Booking.zone_id IS NOT NULL 
								AND Zone.id IS NOT NULL) 
						OR 
							Zone.id IS NULL'
						)
					),
				//recurring within condition
				'conditions' => array(
					'OR' => array(
						'Queue.response <> ' => array('accepted', 'cleared','auto_cancelled'),
						'Queue.response IS NULL AND Queue.id IS NULL', // there is no entry in queues table for these bookings
						),
					'Booking.acceptor IS NULL',
					"(
					((Booking.is_sap_booking <> '1' OR Booking.is_sap_booking IS NULL) AND Booking.status NOT IN ('cancelled_by_passenger', 'request_sent') )
					 OR (Booking.is_sap_booking = '1' AND Booking.status NOT IN ('cancelled_by_passenger', 'request_sent', 'no_driver_found')  ) 
					OR Booking.status IS NULL 
					)",
					"TIMESTAMPDIFF(MINUTE, '$now', Booking.at_time) <= IF(Zone.pickup_lead_time IS NULL, 30, Zone.pickup_lead_time)", // get all booking within next 30 minutes (default)
					),
				'group' => array('Booking.id'),
				'order' => array('Booking.created ASC'),
			)
		); //if no setting is found for specific zone, search booking using default value
		/*if(empty($bookings)){
			$bookings = $this->Queue->Booking->find('all', 
			array(
				'recursive' => 0,
				'joins' => array(
					array(
						'table' => 'queues',
						'alias' => 'Queue',
						'type' => 'LEFT',
						'conditions' => 'Queue.booking_id = Booking.id'
						),
					),
				//recurring within condition
				'conditions' => array(
					'OR' => array(
						'Queue.response <> ' => array('accepted','auto_cancelled'),
						'Queue.response IS NULL AND Queue.id IS NULL', // there is no entry in queues table for these bookings
						),
					'Booking.acceptor IS NULL',
					'Booking.status IS NULL',
					"TIMESTAMPDIFF(MINUTE, '$now', Booking.at_time) <= 30", // get all booking within next 30 minutes 
					),
				'group' => array('Booking.id'),
			)
		);
		} */

		$this->Session->delete('driver_not_found_bookings');

        
		foreach($bookings as $booking) {

		    $booking_id = $booking['Booking']['id'];
            $lat = $booking['Booking']['pick_up_lat'];
            $lng = $booking['Booking']['pick_up_lng'];
            $zone_id = $booking['Booking']['zone_id'];
            $requester = $booking['Booking']['requester'];
            $passenger = $booking['Booking']['persons'];
            $car = $booking['Booking']['car_type'];
            $this->_search_driver($booking_id, $lat, $lng, $requester, $passenger, $car);
            	
		}
		$time_pre = microtime(true);
		$driver_not_found_bookings =  $this->Session->read('driver_not_found_bookings');

		// send push message to all passengers for latest booking only
		foreach ($driver_not_found_bookings as $push_passenger) {
			
			
			$response = array(
					'success' => true,
					'requester' => $push_passenger['requester'],
					'msg' => $push_passenger['msg'],
					'app' => $push_passenger['app'],
					'booking_id' => $push_passenger['booking_id'],
					'phone' => $push_passenger['phone'],
			);
			
			$this->_push($push_passenger['requester'], $push_passenger['msg'], $push_passenger['app'], $push_passenger['booking_id'], $push_passenger['phone']);
		}
		// $this->_apiLog(
		// 	$user_id, 
		// 	var_export($this->request->data, true), 
		// 	var_export($response, true), 
		// 	1, 
		// 	$time_pre);
		//print_r('start_email');
		//$this->_queueCronjob();
		
		$time_post = microtime(true);
		$time = round(($time_post - $GLOBALS['cronStart']));
		//print_r($time . "<br>");
		if($time <= 50) {
			sleep(10);
			$this->booking(1);
			
		}

		$this->clear_log();

		//App::import('Controller','zones');
       	//$cron = new ZonesController();
       	//$cron->cronjobZoneSettings();

       //	$this->test();

	}
	
	public function test(){
		$this->autoRender = false;
		App::uses('CakeEmail', 'Network/Email');
		$Email = new CakeEmail('smtp');
		//$Email->viewVars(array('get_bookings' =>  $get_bookings));
		$Email->emailFormat('html');
		$Email->template('autocancel');
		$Email->to('rob@appinstitute.co.uk');
		$Email->subject('this email is send by booking cron-cabbieapp');
		$Email->send();
	}
	
	//driver  search

	        /* 
'ROUND(3963.0*ACOS(SIN('. $lat .'*PI()/180)*SIN(User.lat*PI()/180)+COS('. $lat .'*PI()/180)*
								COS(User.lat*PI()/180)*COS(('. $lng .'*PI()/180)-(User.lng*PI()/180)))) <= User.running_distance'
        */
        
/*
 *	Scheduling all `timeout` booking
 */
	private function _timeout_bookings(){
		$now = date("Y-m-d H:i:s");
		//default time out after 30 sec
		$this->Queue->query("
			UPDATE queues q
				LEFT JOIN zones zs ON(
					q.zone_id = zs.id
					AND q.zone_id IS NOT NULL
				)
				OR(
					q.zone_id IS NULL
					AND zs.id IS NULL
				)
				SET q.response = 'timeout',
				 q.response_time = '$now'
				WHERE
					TIMESTAMPDIFF(
						SECOND,
						q.request_time,
						'$now'
					)>= IF(zs.req_timeout IS NULL, 30, zs.req_timeout)
				AND q.response IS NULL
				AND q.booking_id IS NOT NULL
			");	

		//change status in booking for req_timeout
			$bookings = $this->Queue->find('all', array(
					'conditions' => array('Queue.response' => 'timeout'),
					'fields' => array('Queue.booking_id'),
				));
			foreach ($bookings as $booking) {
				$booking_id = $booking['Queue']['booking_id'];
				$this->Queue->query("
					UPDATE bookings
						SET status = 'timeout'
						WHERE
						status = 'request_sent' AND id = '$booking_id'
					");	
				
			}

		//log out driver if ignore(timeout) booking
		$reject_threshold_for_zone = $this->Queue->query("SELECT q.zone_id, z.reject_threshold FROM queues q LEFT JOIN zones z ON (q.zone_id = z.id)");
		$reject_threshold =$reject_threshold_for_zone[0]['z']['reject_threshold'];
		
		if(empty($reject_threshold)) {
			$reject_threshold = 1; //set default threshold value 
		}
		
		$drivers = $this->Queue->query("SELECT DISTINCT q.user_id, u.vr_available, u.vr_zone, q.response
			FROM queues q INNER JOIN users u ON (q.user_id = u.id AND q.response = 'timeout')");
		
		foreach ($drivers as $driver) {
			$driver_zone = $driver['u']['vr_zone'];
			$driver_id = $driver['q']['user_id'];
			$reject_count = $this->Queue->query("SELECT
				COUNT(IF(res.response = 'timeout', 1, NULL))AS cnt
			FROM
				(
					SELECT
						response
					FROM
						queues
					WHERE
						user_id = '$driver_id'
					AND response IS NOT NULL
					ORDER BY
						response_time DESC
					LIMIT $reject_threshold
				)AS res");
				
			if($reject_count[0][0]['cnt'] >= $reject_threshold) {
				if ($driver['u']['vr_available'] == 'yes' && $driver['q']['response'] == 'timeout') {
					// send notification to driver
        			$this->_push($driver_id, 'You have been logged-out for ignoring the booking.','CabbieCall', null, null, null, true);
				}
				$this->Queue->query("UPDATE users SET vr_available = 'no', vr_zone=NULL WHERE id = '$driver_id' AND vr_available = 'yes'
				");
				$this->_position_driver($driver_zone);
			} 
		}

	} 


	

}
