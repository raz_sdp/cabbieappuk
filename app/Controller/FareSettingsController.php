<?php
App::uses('AppController', 'Controller');
/**
 * FareSettings Controller
 *
 * @property FareSetting $FareSetting
 * @property PaginatorComponent $Paginator
 */
class FareSettingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('FareSetting.fare_setting_type' => 'fare_by_miles'),
	    );
		$fareSettings = $this->paginate();
	    $this->loadModel('Zone');
	    $zones = $this->Zone->find('list');
		$this->set(compact('fareSettings','zones'));
		//print_r($fareSettings);
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_fixedindex() {
		//$this->FareSetting->recursive = -1;
		$this->paginate = array(
	        'limit' => 25,
	        'conditions' => array('FareSetting.fare_setting_type' => 'fixed_fare'),
	    );
		$fareSettings = $this->paginate();
	    $this->loadModel('Zone');
	    $zones = $this->Zone->find('list');
		$this->set(compact('fareSettings','zones'));
		//print_r($fareSettings);
	}
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FareSetting->exists($id)) {
			throw new NotFoundException(__('Invalid fare setting'));
		}
		$options = array('conditions' => array('FareSetting.' . $this->FareSetting->primaryKey => $id));
		$this->set('fareSetting', $this->FareSetting->find('first', $options));
	}


	// function to add % in fare
	private function _add_percentage($fare){
		if(!empty($fare)) {
			$percent_check = strstr($fare, '%');
			if ($percent_check == false) {
				$fare_per =  $fare. '%';
			} else {
				$fare_per =  $fare;
			}
			return $fare_per;
			
		} else
		{
			return null;
		}
	}
/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if ($this->request->data['FareSetting']['weeks'] != Null) {
				$comma_separated = implode("#", $this->request->data['FareSetting']['weeks']);
				$this->request->data['FareSetting']['weeks'] = '#'. $comma_separated . '#';
			}					
			$distances = $this->request->data['FareSetting']['distance'];
			$fares = $this->request->data['FareSetting']['s_4'];
			$fares_s5 = $this->request->data['FareSetting']['s_5'];
			$fares_s6 = $this->request->data['FareSetting']['s_6'];
			$fares_s7 = $this->request->data['FareSetting']['s_7'];
			$fares_s8 = $this->request->data['FareSetting']['s_8'];
			$fares_e4 = $this->request->data['FareSetting']['e_4'];
			$fares_e5 = $this->request->data['FareSetting']['e_5'];
			$fares_e6 = $this->request->data['FareSetting']['e_6'];
			$fares_e7 = $this->request->data['FareSetting']['e_7'];
			$fares_e8 = $this->request->data['FareSetting']['e_8'];
			$fares_w3 = $this->request->data['FareSetting']['w_3'];
			$fares_w4 = $this->request->data['FareSetting']['w_4'];
			$fares_w5 = $this->request->data['FareSetting']['w_5'];
			$fares_w6 = $this->request->data['FareSetting']['w_6'];
			unset($this->request->data['FareSetting']['distance'], 
				  $this->request->data['FareSetting']['s_4'], 
				  $this->request->data['FareSetting']['s_5'], 
				  $this->request->data['FareSetting']['s_6'], 
				  $this->request->data['FareSetting']['s_7'], 
				  $this->request->data['FareSetting']['s_8'], 
				  $this->request->data['FareSetting']['e_4'],
				  $this->request->data['FareSetting']['e_5'], 
				  $this->request->data['FareSetting']['e_6'], 
				  $this->request->data['FareSetting']['e_7'], 
				  $this->request->data['FareSetting']['e_8'], 
				  $this->request->data['FareSetting']['w_3'], 
				  $this->request->data['FareSetting']['w_4'], 
				  $this->request->data['FareSetting']['w_5'], 
				  $this->request->data['FareSetting']['w_6']);
			$parent = null;
			foreach ($distances as $key => $value) {
					if(!empty($distances[$key]) && !empty($fares[$key])) {
						$this->request->data['FareSetting']['distance'] = $distances[$key];
						$this->request->data['FareSetting']['s_4'] = $fares[$key];
						$this->request->data['FareSetting']['s_5'] = $fares_s5[$key];
						$this->request->data['FareSetting']['s_6'] = $fares_s6[$key];
						$this->request->data['FareSetting']['s_7'] = $fares_s7[$key];
						$this->request->data['FareSetting']['s_8'] = $fares_s8[$key];
						$this->request->data['FareSetting']['e_4'] = $fares_e4[$key];
						$this->request->data['FareSetting']['e_5'] = $fares_e5[$key];
						$this->request->data['FareSetting']['e_6'] = $fares_e6[$key];
						$this->request->data['FareSetting']['e_7'] = $fares_e7[$key];
						$this->request->data['FareSetting']['e_8'] = $fares_e8[$key];
						$this->request->data['FareSetting']['w_3'] = $fares_w3[$key];
						$this->request->data['FareSetting']['w_4'] = $fares_w4[$key];
						$this->request->data['FareSetting']['w_5'] = $fares_w5[$key];
						$this->request->data['FareSetting']['w_6'] = $fares_w6[$key];
						$this->request->data['FareSetting']['parent'] = $parent;
						$this->FareSetting->create();
						$this->FareSetting->save($this->request->data);
						if(empty($parent)) {
							$parent = $this->FareSetting->id;
						}
					}
				}
			if ($this->FareSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The fare setting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fare setting could not be saved. Please, try again.'));
			}
		}

		$zones = $this->FareSetting->Zone->find('list', array(
			'conditions' => array('type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones', 'holidaytypes'));
	}


	public function admin_fixedadd() {
		if ($this->request->is('post')) {
			if ($this->request->data['FareSetting']['weeks'] != Null) {
				$comma_separated = implode("#", $this->request->data['FareSetting']['weeks']);
				$this->request->data['FareSetting']['weeks'] = '#'. $comma_separated . '#';
			}
			$this->FareSetting->create();
			if ($this->FareSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The fare setting has been saved.'));
				return $this->redirect(array('action' => 'fixedindex'));
			} else {
				$this->Session->setFlash(__('The fare setting could not be saved. Please, try again.'));
			}
		}

		$zones = $this->FareSetting->Zone->find('list', array(
			'conditions' => array('type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones', 'holidaytypes'));
	}


/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FareSetting->exists($id)) {
			throw new NotFoundException(__('Invalid fare setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
		//	print_r($this->request->data);
			//exit;
			$this->FareSetting->deleteAll(array('FareSetting.parent' => $id), false);
			if ($this->request->data['FareSetting']['weeks'] != Null) {
				$comma_separated = implode("#", $this->request->data['FareSetting']['weeks']);
				$this->request->data['FareSetting']['weeks'] = '#'. $comma_separated . '#';
			}
			$distances = $this->request->data['FareSetting']['distance'];
			$fares = $this->request->data['FareSetting']['s_4'];
			$fares_s5 = $this->request->data['FareSetting']['s_5'];
			$fares_s6 = $this->request->data['FareSetting']['s_6'];
			$fares_s7 = $this->request->data['FareSetting']['s_7'];
			$fares_s8 = $this->request->data['FareSetting']['s_8'];
			$fares_e4 = $this->request->data['FareSetting']['e_4'];
			$fares_e5 = $this->request->data['FareSetting']['e_5'];
			$fares_e6 = $this->request->data['FareSetting']['e_6'];
			$fares_e7 = $this->request->data['FareSetting']['e_7'];
			$fares_e8 = $this->request->data['FareSetting']['e_8'];
			$fares_w3 = $this->request->data['FareSetting']['w_3'];
			$fares_w4 = $this->request->data['FareSetting']['w_4'];
			$fares_w5 = $this->request->data['FareSetting']['w_5'];
			$fares_w6 = $this->request->data['FareSetting']['w_6'];
			unset($this->request->data['FareSetting']['id'],
				  $this->request->data['FareSetting']['distance'], 
				  $this->request->data['FareSetting']['s_4'], 
				  $this->request->data['FareSetting']['s_5'], 
				  $this->request->data['FareSetting']['s_6'], 
				  $this->request->data['FareSetting']['s_7'], 
				  $this->request->data['FareSetting']['s_8'], 
				  $this->request->data['FareSetting']['e_4'],
				  $this->request->data['FareSetting']['e_5'], 
				  $this->request->data['FareSetting']['e_6'], 
				  $this->request->data['FareSetting']['e_7'], 
				  $this->request->data['FareSetting']['e_8'], 
				  $this->request->data['FareSetting']['w_3'], 
				  $this->request->data['FareSetting']['w_4'], 
				  $this->request->data['FareSetting']['w_5'], 
				  $this->request->data['FareSetting']['w_6']);
			$this->request->data['FareSetting']['distance'] = $distances[0];
			$this->request->data['FareSetting']['s_4'] = $fares[0];
			$this->request->data['FareSetting']['s_5'] = $fares_s5[0];
			$this->request->data['FareSetting']['s_6'] = $fares_s6[0];
			$this->request->data['FareSetting']['s_7'] = $fares_s7[0];
			$this->request->data['FareSetting']['s_8'] = $fares_s8[0];
			$this->request->data['FareSetting']['e_4'] = $fares_e4[0];
			$this->request->data['FareSetting']['e_5'] = $fares_e5[0];
			$this->request->data['FareSetting']['e_6'] = $fares_e6[0];
			$this->request->data['FareSetting']['e_7'] = $fares_e7[0];
			$this->request->data['FareSetting']['e_8'] = $fares_e8[0];
			$this->request->data['FareSetting']['w_3'] = $fares_w3[0];
			$this->request->data['FareSetting']['w_4'] = $fares_w4[0];
			$this->request->data['FareSetting']['w_5'] = $fares_w5[0];
			$this->request->data['FareSetting']['w_6'] = $fares_w6[0];
			$this->FareSetting->id = $id;
			$this->FareSetting->save($this->request->data, false);
			array_shift($distances);
			array_shift($fares);
			array_shift($fares_s5);
			array_shift($fares_s6);
			array_shift($fares_s7);
			array_shift($fares_s8);
			array_shift($fares_e4);
			array_shift($fares_e5);
			array_shift($fares_e6);
			array_shift($fares_e7);
			array_shift($fares_e8);
			array_shift($fares_w3);
			array_shift($fares_w4);
			array_shift($fares_w5);
			array_shift($fares_w6);
			foreach ($distances as $key => $value) {
					if(!empty($distances[$key]) && !empty($fares[$key])) {
						$this->request->data['FareSetting']['distance'] = $distances[$key];
						$this->request->data['FareSetting']['s_4'] = $fares[$key];
						$this->request->data['FareSetting']['s_5'] = $fares_s5[$key];
						$this->request->data['FareSetting']['s_6'] = $fares_s6[$key];
						$this->request->data['FareSetting']['s_7'] = $fares_s7[$key];
						$this->request->data['FareSetting']['s_8'] = $fares_s8[$key];
						$this->request->data['FareSetting']['e_4'] = $fares_e4[$key];
						$this->request->data['FareSetting']['e_5'] = $fares_e5[$key];
						$this->request->data['FareSetting']['e_6'] = $fares_e6[$key];
						$this->request->data['FareSetting']['e_7'] = $fares_e7[$key];
						$this->request->data['FareSetting']['e_8'] = $fares_e8[$key];
						$this->request->data['FareSetting']['w_3'] = $fares_w3[$key];
						$this->request->data['FareSetting']['w_4'] = $fares_w4[$key];
						$this->request->data['FareSetting']['w_5'] = $fares_w5[$key];
						$this->request->data['FareSetting']['w_6'] = $fares_w6[$key];
						$this->request->data['FareSetting']['parent'] = $id;
						$this->FareSetting->create();
						$this->FareSetting->save($this->request->data, false);
					}
				}
			if ($this->FareSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The fare setting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fare setting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FareSetting.' . $this->FareSetting->primaryKey => $id));
			$this->request->data = $this->FareSetting->find('first', $options);
			$children = $this->FareSetting->find('all', array(
					'conditions' => array('FareSetting.parent' => $id),
					'order' => array('FareSetting.id ASC'),
				));
			$this->set(compact('children'));
		}
		$zones = $this->FareSetting->Zone->find('list',array(
			'conditions' => array('type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones', 'holidaytypes'));
	}

	/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_fixededit($id = null) {
		if (!$this->FareSetting->exists($id)) {
			throw new NotFoundException(__('Invalid fare setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->request->data['FareSetting']['weeks'] != Null) {
				$comma_separated = implode("#", $this->request->data['FareSetting']['weeks']);
				$this->request->data['FareSetting']['weeks'] = '#'. $comma_separated . '#';
			}
			if ($this->FareSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The fare setting has been saved.'));
				return $this->redirect(array('action' => 'fixedindex'));
			} else {
				$this->Session->setFlash(__('The fare setting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FareSetting.' . $this->FareSetting->primaryKey => $id));
			$this->request->data = $this->FareSetting->find('first', $options);
		}
		$zones = $this->FareSetting->Zone->find('list',array(
			'conditions' => array('type' => 'vr_zone')
			));
		$this->loadModel('Holidaytype');
		$holidaytypes = $this->Holidaytype->find('list');
		$this->set(compact('zones', 'holidaytypes'));
	}


/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FareSetting->id = $id;
		if (!$this->FareSetting->exists()) {
			throw new NotFoundException(__('Invalid fare setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		$this->FareSetting->deleteAll(array('FareSetting.parent' => $id), false);
		if ($this->FareSetting->delete()) {
			$this->Session->setFlash(__('The fare setting has been deleted.'));
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash(__('The fare setting could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


}
