<?php
App::uses('AppController', 'Controller');

class ApilogsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
  public function admin_index() {
    	$this->Apilog->recursive = 0;
		$this->paginate = array(
	        'limit' => 25,
	        'order' => array('Apilog.created' => 'DESC'),
	    );
	    $apilogs = $this->paginate();
		$this->set(compact('apilogs'));
  }

}