$(function(){
	$('.datePicker').datepicker({dateFormat : 'dd/mm/yy'});

	$('#AddMoreDates').on('click', 
		function(e){
			e.preventDefault();
			$(this).parent().prev().clone().insertBefore($(this).parent());
			$('.datePicker:last').prop('id', '').removeClass('hasDatepicker').datepicker({dateFormat : 'dd/mm/yy'});
		});
	if($('#HolidaytypeIsRepeat').prop('checked')){
		$('#AddMoreDates').hide();
	} else{
		$('#AddMoreDates').show();
	}
	$('#HolidaytypeIsRepeat').on('click',
		function(e){
			if($(this).prop('checked')){
				$('#AddMoreDates').hide();
			} else{
				$('#AddMoreDates').show();
			}
		}
	);

});