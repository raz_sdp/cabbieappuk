var pick_up_postcode = '';
var destination_postcode = '';
jQuery(function () {
    var height = $(window).height();
    $('.all-content, .filter').css({
        "min-height": height

    });
    $('.tabs-list').css({
        "min-height": height,
        "height": "auto",
        "background": "#3d4045"
    });
    $('.login-arrow span').on('click', function () {
        $('.login-form').toggle();
    });
    $('#sideMenu a').on('shown.bs.tab', function (e) {

        if (e.target.href.indexOf('chart') !== -1) {
            renderCharts();
        }
        // if(e.relatedTarget.indexOf('chart')!== -1){

        // }// previous tab
    });
    $('#sideMenu a').on('shown.bs.tab', function (e) {

        if (e.target.href.indexOf('SummaryCharts') !== -1) {
            SummarypieCharts();
        }
        // if(e.relatedTarget.indexOf('chart')!== -1){

        // }// previous tab
    });

    $('.part-three input[type="checkbox"]').click(function () {
        $('.part-three input[type="checkbox"]').not(this).prop('checked', false).prev().hide().prev().show();
        if ($(this).is(':checked'))
            $(this).prev().show().prev().hide(); // checked
        else
            $(this).prev().hide().prev().show();
    });
    $('#asap input[type="checkbox"]').click(function () {
        if ($(this).is(':checked')) {
            $(this).prev().show().prev().hide();
            $('#datetext').val('');
            $('#timetext').val('');
            $("#datetext").datepicker('disable');
            $('#timetext').prop('disabled', true);
        } // checked
        else {
            $(this).prev().hide().prev().show();
            $("#datetext").datepicker('enable');
            $('#timetext').prop('disabled', false);
        }

    });
    //
    $('.jscrollpane').jScrollPane();
    //
    $("#datetext").datepicker({
        dateFormat: 'dd/mm/yy'
    });
    $('#timetext').timepicker();
    //parallax
    $('#intro').parallax("50%", 0.1);
    $('#second').parallax("50%", 0.1);
    $('#third').parallax("50%", 0.3);
    $('.login-link').click(function () {
        if (loged_in == false) {
            $(':input', '.popup').val('');
            $('.popup, #overlay').show();
            loginClicked = true;
        } else {
            logout();
        }
    });
    $('.signup-link').click(function () {
        $(':input', '.popup').val('');
        $('.popup, #overlay').show();
    });
    $('.delete').click(function () {
        $('.popup, #overlay').hide();
        loginClicked = false;
    });

    // autocomplete
    var onDestinationAutocompleteSelected = function (event, ui) {
        if ($(event.target).prop('id') == 'destination')
            destination_postcode = $("#destination").val();

        $(event.target).val(ui.item.address);
        return false;
    };

    var renderDestinationAddressList = function (ul, item) {
        if (item.address) {
            return $("<li>")
                .append("<a>" + item.address + "</a>")
                .appendTo(ul);
        } else {
            return false;
        }

    };



    $(".postcode-auto-complete").autocomplete({
        source: ROOT + 'zones/proxy',
        minLength: 1,
        select: onDestinationAutocompleteSelected
    }).each(function(){
        $(this).data("ui-autocomplete")._renderItem = renderDestinationAddressList;
    });


    var via = $('#via').html();
    $('#via').empty();
    $('.add-via').click(function () {
        $('.another-des').show().css('display', 'inline-block');
        $('#via').append(via);
        $(".postcode-auto-complete:last").autocomplete({
            source: ROOT + 'zones/proxy',
            minLength: 1,
            select: onDestinationAutocompleteSelected
        }).data("ui-autocomplete")._renderItem = renderDestinationAddressList;
    });

    /*$("#pickup").autocomplete({
        source: ROOT + 'zones/proxy',
        minLength: 1,
        select: function (event, ui) {
            pick_up_postcode = $("#pickup").val();
            $("#pickup").val(ui.item.address);
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        if (item.address) {
            return $("<li>")
                .append("<a>" + item.address + "</a>")
                .appendTo(ul);
        } else {
            return false;
        }

    };*/




    /*
    $( "#destination1" ).autocomplete({
      source: ROOT + 'zones/proxy',
      minLength: 1,
      select: function( event, ui ) {
        destination_postcode = $( "#destination" ).val();       
        $( "#destination" ).val( ui.item.address );
        return false;
      }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        if(item.address){
             return $( "<li>" )
        .append( "<a>" + item.address + "</a>" )
        .appendTo( ul );
    } else {
        return false;
    }
     
    };
    */

    $('.dwn-iphone').on('click', function () {
        location.href = itunes_link;
    });
    $('.dwn-and').on('click', function () {
        location.href = android;
    });

});