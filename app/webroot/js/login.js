$(function() {
    $('#loginForm').on('submit',function(e) {
    	e.preventDefault();
        $.post(ROOT + 'users/driver_login', $(this).serialize(), function(res) {  

        	if(res.success){
          		window.location.href = ROOT + 'pages/topup/'+res.User.id;
                //alert(res);
        	} else {
        		alert(res.message);
        	}
            //console.log(res);
        }, 'json');
    });
});