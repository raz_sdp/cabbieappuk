$(function() {
	$('#signimg').click(function() {
		var name = $('#name').val();
		var mobileno = $('#mobileno').val();
		var email = $('#signemail').val();
		var password = $('#signpassword').val();
		var confirmpwd = $('#cp').val();
		var filter = /^[0-9-+]+$/;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!filter.test(mobileno)) {
			alert('Invalid Mobile No');
			return;
		}
		if (!emailReg.test(email)) {
			alert('Invalid Email');
			return;
		}
		if (!name) {
			alert('Please enter your Name');
			return;
		}
		if (!password) {
			alert('Please enter your Password');
			return;
		}
		if (!confirmpwd) {
			alert('Please Confirm Your Password');
			return;
		}
		$.post(ROOT + 'users/register', {
			'data[User][type]': 'passenger',
			'data[User][name]': name,
			'data[User][mobile]': mobileno,
			'data[User][email]': email,
			'data[User][password]': password,
			'data[User][passwordx]': confirmpwd,
			'data[User][from_web]': '1'
		},
		function(res) {
			if (res.success) {
				$('.popup, #overlay').hide();
				loginClicked = false;				
				$.post(ROOT + 'users/sendto_passenger', {
					'data[User][mobile]': mobileno,
					'data[User][name]': name,
					'data[User][email]': email
				},
				function(res) {},
				'json');
			} else {
				alert(res.message);
			}
		},
		'json');
	});
});