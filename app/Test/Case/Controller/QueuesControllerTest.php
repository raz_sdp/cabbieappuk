<?php
App::uses('QueuesController', 'Controller');

/**
 * QueuesController Test Case
 *
 */
class QueuesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.queue',
		'app.virtual_rank',
		'app.user',
		'app.vehicle',
		'app.billing',
		'app.promoter',
		'app.areacode',
		'app.areacodes_promoter',
		'app.booking',
		'app.reject',
		'app.request_log',
		'app.booking_setting',
		'app.areacodes',
		'app.virtual_ranks',
		'app.fare_setting',
		'app.notification_log',
		'app.top_up',
		'app.tracking',
		'app.trouble_log'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
