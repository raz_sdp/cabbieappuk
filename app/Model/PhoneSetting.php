<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class PhoneSetting extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	// public $validate = array(
	// 	'req_timeout' => array(
	// 		'notempty' => array(
	// 			'rule' => array('comparison', '>=', 60),
	// 			'message' => 'Minimum seconds must be greater than 59',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// );

	

	public $belongsTo = array(
		'Zone' => array(
			'className' => 'Zone',
			'foreignKey' => 'zone_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
